05.02.2018 21:38:30 - Preparing Test Run metadata_00626031_Slovensk&aacute;agent&uacute;raživotn&eacute;hoprostredia_Environment&aacute;lne z&aacute;ťaže Slovenskej republiky - Register EZ (initiated Mon Feb 05 21:38:30 CET 2018)
05.02.2018 21:38:30 - Resolving Executable Test Suite dependencies
05.02.2018 21:38:30 - Preparing 2 Test Task:
05.02.2018 21:38:30 -  TestTask 1 (5d7fec26-1d0e-48f7-b61b-14b7b188e1bb)
05.02.2018 21:38:30 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
05.02.2018 21:38:30 -  with parameters: 
05.02.2018 21:38:30 - etf.testcases = *
05.02.2018 21:38:30 -  TestTask 2 (4f6b29da-8205-4a14-8a0f-0ac7197e0431)
05.02.2018 21:38:30 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
05.02.2018 21:38:30 -  with parameters: 
05.02.2018 21:38:30 - etf.testcases = *
05.02.2018 21:38:30 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
05.02.2018 21:38:30 - Setting state to CREATED
05.02.2018 21:38:30 - Changed state from CREATED to INITIALIZING
05.02.2018 21:38:30 - Starting TestRun.f49545d7-4975-4959-a7d9-cefa09e98599 at 2018-02-05T21:38:32+01:00
05.02.2018 21:38:32 - Changed state from INITIALIZING to INITIALIZED
05.02.2018 21:38:32 - TestRunTask initialized
05.02.2018 21:38:32 - Creating new tests databases to speed up tests.
05.02.2018 21:38:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:38:32 - Optimizing last database etf-tdb-1b89963a-926b-4e89-827b-df981f7ebbb2-0 
05.02.2018 21:38:32 - Import completed
05.02.2018 21:38:32 - Validation ended with 0 error(s)
05.02.2018 21:38:32 - Compiling test script
05.02.2018 21:38:32 - Starting XQuery tests
05.02.2018 21:38:32 - "Testing 1 records"
05.02.2018 21:38:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
05.02.2018 21:38:32 - "Statistics table: 0 ms"
05.02.2018 21:38:32 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
05.02.2018 21:38:32 - "Test Case 'Schema validation' started"
05.02.2018 21:38:33 - "Validating file GetRecordByIdResponse.xml: 1503 ms"
05.02.2018 21:38:33 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1503 ms"
05.02.2018 21:38:33 - "Test Case 'Schema validation' finished: PASSED"
05.02.2018 21:38:33 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
05.02.2018 21:38:34 - Releasing resources
05.02.2018 21:38:34 - TestRunTask initialized
05.02.2018 21:38:34 - Recreating new tests databases as the Test Object has changed!
05.02.2018 21:38:34 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:38:34 - Optimizing last database etf-tdb-1b89963a-926b-4e89-827b-df981f7ebbb2-0 
05.02.2018 21:38:34 - Import completed
05.02.2018 21:38:34 - Validation ended with 0 error(s)
05.02.2018 21:38:34 - Compiling test script
05.02.2018 21:38:34 - Starting XQuery tests
05.02.2018 21:38:34 - "Testing 1 records"
05.02.2018 21:38:34 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
05.02.2018 21:38:34 - "Statistics table: 1 ms"
05.02.2018 21:38:34 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
05.02.2018 21:38:34 - "Test Case 'Common tests' started"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Case 'Common tests' finished: PASSED_MANUAL"
05.02.2018 21:38:34 - "Test Case 'Hierarchy level' started"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 1 ms"
05.02.2018 21:38:34 - "Test Case 'Hierarchy level' finished: PASSED"
05.02.2018 21:38:34 - "Test Case 'Dataset (series) tests' started"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
05.02.2018 21:38:34 - "Checking URL: 'http://envirozataze.enviroportal.sk/LicenciaAtributoveUdaje.aspx'"
05.02.2018 21:38:34 - "Checking URL: 'http://envirozataze.enviroportal.sk/LicenciaPriestoroveUdaje.aspx'"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 372 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 1 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Case 'Dataset (series) tests' finished: PASSED_MANUAL"
05.02.2018 21:38:34 - "Test Case 'Service tests' started"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Case 'Service tests' finished: PASSED"
05.02.2018 21:38:34 - "Test Case 'Keywords' started"
05.02.2018 21:38:34 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
05.02.2018 21:38:34 - "Test Case 'Keywords' finished: PASSED"
05.02.2018 21:38:34 - "Test Case 'Keywords - details' started"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
05.02.2018 21:38:34 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
05.02.2018 21:38:35 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.f.1: Dataset keyword': FAILED - 554 ms"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': FAILED - 0 ms"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
05.02.2018 21:38:35 - "Test Case 'Keywords - details' finished: FAILED"
05.02.2018 21:38:35 - "Test Case 'Temporal extent' started"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
05.02.2018 21:38:35 - "Test Case 'Temporal extent' finished: PASSED"
05.02.2018 21:38:35 - "Test Case 'Temporal extent - details' started"
05.02.2018 21:38:35 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 1 ms"
05.02.2018 21:38:35 - "Test Case 'Temporal extent - details' finished: PASSED"
05.02.2018 21:38:35 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
05.02.2018 21:38:35 - Releasing resources
05.02.2018 21:38:35 - Changed state from INITIALIZED to RUNNING
05.02.2018 21:38:35 - Duration: 5sec
05.02.2018 21:38:35 - TestRun finished
05.02.2018 21:38:35 - Changed state from RUNNING to COMPLETED
