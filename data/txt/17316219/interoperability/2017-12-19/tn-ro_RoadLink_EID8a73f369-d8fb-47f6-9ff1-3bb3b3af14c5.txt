19.12.2017 23:04:21 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_tn-ro:RoadLink (initiated Tue Dec 19 23:04:21 CET 2017)
19.12.2017 23:04:21 - Resolving Executable Test Suite dependencies
19.12.2017 23:04:22 - Preparing 10 Test Task:
19.12.2017 23:04:22 -  TestTask 1 (231c75e3-df53-4f17-ae13-26d8927a6a65)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: GML application schemas, Transport Networks (EID: 9af1c865-1cf0-43ff-9250-069df01b0948, V: 0.2.1 )'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 2 (97d7d969-926c-41c9-a36c-6f5d36f2917b)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 3 (af87ad07-037b-4351-b611-3642dcb99596)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Road Transport Networks (EID: 14986e54-74c4-43b0-979b-d0d3e5cd0e8c, V: 0.2.1 )'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 4 (39f559ca-010a-4194-af62-dcc1576c6d50)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 5 (fe76b915-7745-49e8-8099-2cacb2128724)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 6 (1739a5e6-607e-4783-a676-0068fc91aee9)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 7 (b67597cd-368a-496d-80d3-5a95b93633c5)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 8 (69d5a664-a9bc-4e17-b479-d263865b888d)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.1 )'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 9 (f3e0a34e-0f4f-470f-87f3-e4917b8f2a09)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 -  TestTask 10 (a9658410-7f67-4033-80d9-27af2ca50b29)
19.12.2017 23:04:22 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
19.12.2017 23:04:22 -  with parameters: 
19.12.2017 23:04:22 - etf.testcases = *
19.12.2017 23:04:22 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
19.12.2017 23:04:22 - Setting state to CREATED
19.12.2017 23:04:22 - Changed state from CREATED to INITIALIZING
19.12.2017 23:04:22 - Starting TestRun.8a73f369-d8fb-47f6-9ff1-3bb3b3af14c5 at 2017-12-19T23:04:23+01:00
19.12.2017 23:04:23 - Changed state from INITIALIZING to INITIALIZED
19.12.2017 23:04:23 - TestRunTask initialized
19.12.2017 23:04:23 - Creating new tests databases to speed up tests.
19.12.2017 23:04:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:23 - Optimizing last database etf-tdb-a3a6b580-20f3-440e-b47a-0a83f38072fb-0 
19.12.2017 23:04:24 - Import completed
19.12.2017 23:04:25 - Validation ended with 0 error(s)
19.12.2017 23:04:25 - Compiling test script
19.12.2017 23:04:25 - Starting XQuery tests
19.12.2017 23:04:25 - "Testing 1 features"
19.12.2017 23:04:25 - "Indexing features (parsing errors: 0): 104 ms"
19.12.2017 23:04:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-gml/ets-tn-gml-bsxets.xml"
19.12.2017 23:04:25 - "Statistics table: 1 ms"
19.12.2017 23:04:25 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' started"
19.12.2017 23:04:25 - "Test Case 'Basic test' started"
19.12.2017 23:04:25 - "Test Assertion 'tn-gml.a.1: Transport Network feature in dataset': PASSED - 1 ms"
19.12.2017 23:04:25 - "Test Case 'Basic test' finished: PASSED"
19.12.2017 23:04:25 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' finished: PASSED"
19.12.2017 23:04:26 - Releasing resources
19.12.2017 23:04:26 - TestRunTask initialized
19.12.2017 23:04:26 - Recreating new tests databases as the Test Object has changed!
19.12.2017 23:04:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:26 - Optimizing last database etf-tdb-a3a6b580-20f3-440e-b47a-0a83f38072fb-0 
19.12.2017 23:04:26 - Import completed
19.12.2017 23:04:26 - Validation ended with 0 error(s)
19.12.2017 23:04:26 - Compiling test script
19.12.2017 23:04:26 - Starting XQuery tests
19.12.2017 23:04:26 - "Testing 1 features"
19.12.2017 23:04:26 - "Indexing features (parsing errors: 0): 132 ms"
19.12.2017 23:04:26 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-as/ets-tn-as-bsxets.xml"
19.12.2017 23:04:26 - "Statistics table: 1 ms"
19.12.2017 23:04:26 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' started"
19.12.2017 23:04:26 - "Test Case 'Code list values' started"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.a.1: AccessRestrictionValue attributes': PASSED - 61 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.a.2: RestrictionTypeValue attributes': PASSED - 15 ms"
19.12.2017 23:04:26 - "Test Case 'Code list values' finished: PASSED"
19.12.2017 23:04:26 - "Test Case 'Constraints' started"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.1: TrafficFlowDirection can only be associated with a spatial object of the type Link or LinkSequence.': PASSED - 1 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.2: All transport areas have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.3: All transport links have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.4: All transport link sequences have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.5: All transport link sets have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.6: All transport nodes have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.7: All transport points have an external object identifier.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.8: All transport properties have an external object identifier.': PASSED - 1 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.9: A transport link sequence must be composed of transport links that all belong to the same transport network.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.b.10: A transport link set must be composed of transport links and or transport link sequences that all belong to the same transport network.': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Case 'Constraints' finished: PASSED"
19.12.2017 23:04:26 - "Test Case 'Geometry' started"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.c.1: No free transport nodes': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.c.2: Intersections only at crossings': PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Case 'Geometry' finished: PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Case 'Network connectivity' started"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.d.1: Connectivity at crossings': PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.d.2: Unconnected nodes': PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.d.3: Connectivity tolerance': PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Case 'Network connectivity' finished: PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Case 'Object References' started"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.e.1: Linear references': PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Assertion 'tn-as.e.2: Inter-modal connections': PASSED - 0 ms"
19.12.2017 23:04:26 - "Test Case 'Object References' finished: PASSED_MANUAL"
19.12.2017 23:04:26 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' finished: PASSED_MANUAL"
19.12.2017 23:04:27 - Releasing resources
19.12.2017 23:04:27 - TestRunTask initialized
19.12.2017 23:04:27 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:27 - Validation ended with 0 error(s)
19.12.2017 23:04:27 - Compiling test script
19.12.2017 23:04:27 - Starting XQuery tests
19.12.2017 23:04:27 - "Testing 1 features"
19.12.2017 23:04:27 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ro-as/ets-tn-ro-as-bsxets.xml"
19.12.2017 23:04:27 - "Statistics table: 1 ms"
19.12.2017 23:04:27 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' started"
19.12.2017 23:04:27 - "Test Case 'Code list values' started"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.1: AreaConditionValue attributes': PASSED - 13 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.2: FormOfRoadNodeValue attributes': PASSED - 19 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.3: FormOfWayValue attributes': PASSED - 9 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.4: RoadPartValue attributes': PASSED - 6 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.5: RoadServiceTypeValue attributes': PASSED - 6 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.6: RoadSurfaceCategoryValue attributes': PASSED - 6 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.7: ServiceFacilityValue attributes': PASSED - 8 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.8: SpeedLimitSourceValue attributes': PASSED - 10 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.9: VehicleTypeValue attributes': PASSED - 25 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.a.10: WeatherConditionValue attributes': PASSED - 13 ms"
19.12.2017 23:04:27 - "Test Case 'Code list values' finished: PASSED"
19.12.2017 23:04:27 - "Test Case 'Constraints' started"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.1: FormOfWay can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.2: FunctionalRoadClass can only be associated with a spatial object that is part of a road transport network.': PASSED - 1 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.3: NumberOfLanes can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.4: RoadName can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.5: RoadSurfaceCategory can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.6: RoadWidth can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.7: SpeedLimit can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.b.8: RoadServiceType can only be associated with a spatial object of the type RoadServiceArea or RoadNode (when formOfRoadNode=roadServiceArea)': PASSED - 0 ms"
19.12.2017 23:04:27 - "Test Case 'Constraints' finished: PASSED"
19.12.2017 23:04:27 - "Test Case 'Link centrelines' started"
19.12.2017 23:04:27 - "Test Assertion 'tn-ro-as.c.1: Link centrelines test': PASSED_MANUAL"
19.12.2017 23:04:27 - "Test Case 'Link centrelines' finished: PASSED_MANUAL"
19.12.2017 23:04:27 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' finished: PASSED_MANUAL"
19.12.2017 23:04:28 - Releasing resources
19.12.2017 23:04:28 - TestRunTask initialized
19.12.2017 23:04:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:28 - Validation ended with 0 error(s)
19.12.2017 23:04:28 - Compiling test script
19.12.2017 23:04:28 - Starting XQuery tests
19.12.2017 23:04:28 - "Testing 1 features"
19.12.2017 23:04:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
19.12.2017 23:04:28 - "Statistics table: 0 ms"
19.12.2017 23:04:28 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
19.12.2017 23:04:28 - "Test Case 'Basic tests' started"
19.12.2017 23:04:28 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
19.12.2017 23:04:28 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
19.12.2017 23:04:28 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
19.12.2017 23:04:28 - "Test Case 'Basic tests' finished: PASSED"
19.12.2017 23:04:28 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
19.12.2017 23:04:28 - Releasing resources
19.12.2017 23:04:28 - TestRunTask initialized
19.12.2017 23:04:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:28 - Validation ended with 0 error(s)
19.12.2017 23:04:28 - Compiling test script
19.12.2017 23:04:28 - Starting XQuery tests
19.12.2017 23:04:28 - "Testing 1 features"
19.12.2017 23:04:29 - "Indexing features (parsing errors: 0): 127 ms"
19.12.2017 23:04:29 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
19.12.2017 23:04:29 - "Statistics table: 0 ms"
19.12.2017 23:04:29 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
19.12.2017 23:04:29 - "Test Case 'Version consistency' started"
19.12.2017 23:04:29 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
19.12.2017 23:04:29 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
19.12.2017 23:04:29 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
19.12.2017 23:04:29 - "Test Case 'Temporal consistency' started"
19.12.2017 23:04:29 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Case 'Temporal consistency' finished: PASSED"
19.12.2017 23:04:29 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
19.12.2017 23:04:29 - Releasing resources
19.12.2017 23:04:29 - TestRunTask initialized
19.12.2017 23:04:29 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:29 - Validation ended with 0 error(s)
19.12.2017 23:04:29 - Compiling test script
19.12.2017 23:04:29 - Starting XQuery tests
19.12.2017 23:04:29 - "Testing 1 features"
19.12.2017 23:04:29 - "Indexing features (parsing errors: 0): 65 ms"
19.12.2017 23:04:29 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-dc/ets-tn-dc-bsxets.xml"
19.12.2017 23:04:29 - "Statistics table: 0 ms"
19.12.2017 23:04:29 - "Test Suite 'Conformance class: Data consistency, Transport Networks' started"
19.12.2017 23:04:29 - "Test Case 'Spatial consistency' started"
19.12.2017 23:04:29 - "Test Assertion 'tn-dc.a.1: Each Transport Network Link or Node geometry is within a Network Area geometry (Road Transport Networks)': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Assertion 'tn-dc.a.2: Each Transport Network Link or Node geometry is within a Network Area geometry (Railway Transport Networks)': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Assertion 'tn-dc.a.3: Each Transport Network Link or Node geometry is within a Network Area geometry (Waterway Transport Networks)': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Assertion 'tn-dc.a.4: Each Transport Network Link or Node geometry is within a Network Area geometry (Air Transport Networks)': PASSED - 0 ms"
19.12.2017 23:04:29 - "Test Assertion 'tn-dc.a.5: Manual review': PASSED_MANUAL"
19.12.2017 23:04:29 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
19.12.2017 23:04:29 - "Test Suite 'Conformance class: Data consistency, Transport Networks' finished: PASSED_MANUAL"
19.12.2017 23:04:30 - Releasing resources
19.12.2017 23:04:30 - TestRunTask initialized
19.12.2017 23:04:30 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:30 - Validation ended with 0 error(s)
19.12.2017 23:04:30 - Compiling test script
19.12.2017 23:04:30 - Starting XQuery tests
19.12.2017 23:04:30 - "Testing 1 features"
19.12.2017 23:04:30 - "Indexing features (parsing errors: 0): 139 ms"
19.12.2017 23:04:30 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
19.12.2017 23:04:30 - "Statistics table: 1 ms"
19.12.2017 23:04:30 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
19.12.2017 23:04:30 - "Test Case 'Coordinate reference systems (CRS)' started"
19.12.2017 23:04:30 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
19.12.2017 23:04:30 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
19.12.2017 23:04:30 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
19.12.2017 23:04:30 - Releasing resources
19.12.2017 23:04:30 - TestRunTask initialized
19.12.2017 23:04:30 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:30 - Validation ended with 0 error(s)
19.12.2017 23:04:30 - Compiling test script
19.12.2017 23:04:30 - Starting XQuery tests
19.12.2017 23:04:30 - "Testing 1 features"
19.12.2017 23:04:30 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ia/ets-tn-ia-bsxets.xml"
19.12.2017 23:04:30 - "Statistics table: 0 ms"
19.12.2017 23:04:30 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' started"
19.12.2017 23:04:30 - "Test Case 'Code lists' started"
19.12.2017 23:04:30 - "Test Assertion 'tn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
19.12.2017 23:04:30 - "Test Case 'Code lists' finished: PASSED"
19.12.2017 23:04:30 - "Test Case 'Feature references' started"
19.12.2017 23:04:30 - "Test Assertion 'tn-ia.b.1: MarkerPost.route': PASSED - 9 ms"
19.12.2017 23:04:30 - "Test Assertion 'tn-ia.b.2: TransportLinkSet.post': PASSED - 0 ms"
19.12.2017 23:04:30 - "Test Case 'Feature references' finished: PASSED"
19.12.2017 23:04:30 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' finished: PASSED"
19.12.2017 23:04:31 - Releasing resources
19.12.2017 23:04:31 - TestRunTask initialized
19.12.2017 23:04:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:31 - Validation ended with 0 error(s)
19.12.2017 23:04:31 - Compiling test script
19.12.2017 23:04:31 - Starting XQuery tests
19.12.2017 23:04:31 - "Testing 1 features"
19.12.2017 23:04:31 - "Indexing features (parsing errors: 0): 47 ms"
19.12.2017 23:04:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
19.12.2017 23:04:31 - "Statistics table: 0 ms"
19.12.2017 23:04:31 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
19.12.2017 23:04:31 - "Test Case 'Spatial reference systems' started"
19.12.2017 23:04:31 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
19.12.2017 23:04:31 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
19.12.2017 23:04:31 - "Test Case 'Spatial reference systems' finished: FAILED"
19.12.2017 23:04:31 - "Test Case 'Temporal reference systems' started"
19.12.2017 23:04:31 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 1 ms"
19.12.2017 23:04:31 - "Test Case 'Temporal reference systems' finished: PASSED"
19.12.2017 23:04:31 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
19.12.2017 23:04:31 - Releasing resources
19.12.2017 23:04:31 - TestRunTask initialized
19.12.2017 23:04:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:04:31 - Validation ended with 0 error(s)
19.12.2017 23:04:31 - Compiling test script
19.12.2017 23:04:31 - Starting XQuery tests
19.12.2017 23:04:31 - "Testing 1 features"
19.12.2017 23:04:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-rs/ets-tn-rs-bsxets.xml"
19.12.2017 23:04:31 - "Statistics table: 0 ms"
19.12.2017 23:04:31 - "Test Suite 'Conformance class: Reference systems, Transport Networks' started"
19.12.2017 23:04:31 - "Test Case 'Additional theme-specific rules for reference systems' started"
19.12.2017 23:04:31 - "Test Assertion 'tn-rs.a.1: Test always passes': PASSED - 0 ms"
19.12.2017 23:04:31 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
19.12.2017 23:04:31 - "Test Suite 'Conformance class: Reference systems, Transport Networks' finished: PASSED"
19.12.2017 23:04:31 - Releasing resources
19.12.2017 23:04:31 - Changed state from INITIALIZED to RUNNING
19.12.2017 23:04:32 - Duration: 9sec
19.12.2017 23:04:32 - TestRun finished
19.12.2017 23:04:32 - Changed state from RUNNING to COMPLETED
