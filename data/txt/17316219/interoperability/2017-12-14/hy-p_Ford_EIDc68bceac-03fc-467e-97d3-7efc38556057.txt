14.12.2017 21:49:10 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_hy-p:Ford (initiated Thu Dec 14 21:49:10 CET 2017)
14.12.2017 21:49:10 - Resolving Executable Test Suite dependencies
14.12.2017 21:49:10 - Preparing 10 Test Task:
14.12.2017 21:49:10 -  TestTask 1 (f99321e4-9088-43fc-be67-9c2adbc8e665)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 2 (d510e3dc-2604-4e2c-b9ed-9b0c51ae11c1)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 3 (887cf137-9250-425d-9b79-a35df42f491c)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 4 (2c01db76-0a89-4c25-855f-af9be80521a8)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 5 (8e8abf8b-1770-45a4-a45a-9ca2edc8157b)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 6 (1bbf8d8d-060c-4a7e-a15d-e16cea2b16a3)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 7 (ec09dfd7-59c3-465a-9e3b-78ab7c76ba5b)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 8 (3f635cf8-f4f0-4b5c-a5fb-6cc9515bceed)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 9 (ef341800-c592-4a97-a95c-39d436703d56)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 -  TestTask 10 (1437e5e6-5481-44d2-a8af-d63b3e57ef0b)
14.12.2017 21:49:10 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
14.12.2017 21:49:10 -  with parameters: 
14.12.2017 21:49:10 - etf.testcases = *
14.12.2017 21:49:10 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
14.12.2017 21:49:10 - Setting state to CREATED
14.12.2017 21:49:10 - Changed state from CREATED to INITIALIZING
14.12.2017 21:49:11 - Starting TestRun.c68bceac-03fc-467e-97d3-7efc38556057 at 2017-12-14T21:49:12+01:00
14.12.2017 21:49:12 - Changed state from INITIALIZING to INITIALIZED
14.12.2017 21:49:12 - TestRunTask initialized
14.12.2017 21:49:12 - Creating new tests databases to speed up tests.
14.12.2017 21:49:12 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:12 - Optimizing last database etf-tdb-437f3775-1977-49c3-9720-02ca0f7f2b1d-0 
14.12.2017 21:49:12 - Import completed
14.12.2017 21:49:14 - Validation ended with 0 error(s)
14.12.2017 21:49:14 - Compiling test script
14.12.2017 21:49:14 - Starting XQuery tests
14.12.2017 21:49:14 - "Testing 1 features"
14.12.2017 21:49:14 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
14.12.2017 21:49:14 - "Statistics table: 0 ms"
14.12.2017 21:49:14 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
14.12.2017 21:49:14 - "Test Case 'Basic tests' started"
14.12.2017 21:49:14 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
14.12.2017 21:49:14 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
14.12.2017 21:49:14 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
14.12.2017 21:49:14 - "Test Case 'Basic tests' finished: PASSED"
14.12.2017 21:49:14 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
14.12.2017 21:49:14 - Releasing resources
14.12.2017 21:49:14 - TestRunTask initialized
14.12.2017 21:49:14 - Recreating new tests databases as the Test Object has changed!
14.12.2017 21:49:14 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:15 - Optimizing last database etf-tdb-437f3775-1977-49c3-9720-02ca0f7f2b1d-0 
14.12.2017 21:49:15 - Import completed
14.12.2017 21:49:15 - Validation ended with 0 error(s)
14.12.2017 21:49:15 - Compiling test script
14.12.2017 21:49:15 - Starting XQuery tests
14.12.2017 21:49:15 - "Testing 1 features"
14.12.2017 21:49:15 - "Indexing features (parsing errors: 0): 35 ms"
14.12.2017 21:49:15 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
14.12.2017 21:49:15 - "Statistics table: 1 ms"
14.12.2017 21:49:15 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
14.12.2017 21:49:15 - "Test Case 'Schema' started"
14.12.2017 21:49:15 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
14.12.2017 21:49:15 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
14.12.2017 21:49:15 - "Test Case 'Schema' finished: PASSED_MANUAL"
14.12.2017 21:49:15 - "Test Case 'Schema validation' started"
14.12.2017 21:49:15 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 1 ms"
14.12.2017 21:49:15 - "Validating get.xml"
14.12.2017 21:49:22 - "Duration: 7612 ms. Errors: 0."
14.12.2017 21:49:22 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 7612 ms"
14.12.2017 21:49:22 - "Test Case 'Schema validation' finished: PASSED"
14.12.2017 21:49:22 - "Test Case 'GML model' started"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Case 'GML model' finished: PASSED"
14.12.2017 21:49:22 - "Test Case 'Simple features' started"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 27 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 1 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 23 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Case 'Simple features' finished: PASSED"
14.12.2017 21:49:22 - "Test Case 'Code list values in basic data types' started"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 2 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 2 ms"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
14.12.2017 21:49:22 - "Test Case 'Code list values in basic data types' finished: PASSED"
14.12.2017 21:49:22 - "Test Case 'Constraints' started"
14.12.2017 21:49:22 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
14.12.2017 21:49:22 - "Test Case 'Constraints' finished: PASSED"
14.12.2017 21:49:22 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
14.12.2017 21:49:24 - Releasing resources
14.12.2017 21:49:24 - TestRunTask initialized
14.12.2017 21:49:24 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:24 - Validation ended with 0 error(s)
14.12.2017 21:49:24 - Compiling test script
14.12.2017 21:49:24 - Starting XQuery tests
14.12.2017 21:49:24 - "Testing 1 features"
14.12.2017 21:49:24 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
14.12.2017 21:49:24 - "Statistics table: 1 ms"
14.12.2017 21:49:24 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
14.12.2017 21:49:24 - "Test Case 'Basic test' started"
14.12.2017 21:49:24 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
14.12.2017 21:49:24 - "Test Case 'Basic test' finished: PASSED"
14.12.2017 21:49:24 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
14.12.2017 21:49:24 - Releasing resources
14.12.2017 21:49:24 - TestRunTask initialized
14.12.2017 21:49:24 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:24 - Validation ended with 0 error(s)
14.12.2017 21:49:24 - Compiling test script
14.12.2017 21:49:25 - Starting XQuery tests
14.12.2017 21:49:25 - "Testing 1 features"
14.12.2017 21:49:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
14.12.2017 21:49:25 - "Statistics table: 0 ms"
14.12.2017 21:49:25 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
14.12.2017 21:49:25 - "Test Case 'Code list values' started"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 6 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 8 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 8 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 8 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 4 ms"
14.12.2017 21:49:25 - "Test Case 'Code list values' finished: PASSED"
14.12.2017 21:49:25 - "Test Case 'Geometry' started"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Case 'Geometry' finished: PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Case 'Identifiers and references' started"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Case 'Constraints' started"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
14.12.2017 21:49:25 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Case 'Constraints' finished: PASSED_MANUAL"
14.12.2017 21:49:25 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
14.12.2017 21:49:26 - Releasing resources
14.12.2017 21:49:26 - TestRunTask initialized
14.12.2017 21:49:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:26 - Validation ended with 0 error(s)
14.12.2017 21:49:26 - Compiling test script
14.12.2017 21:49:26 - Starting XQuery tests
14.12.2017 21:49:26 - "Testing 1 features"
14.12.2017 21:49:26 - "Indexing features (parsing errors: 0): 95 ms"
14.12.2017 21:49:26 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
14.12.2017 21:49:26 - "Statistics table: 0 ms"
14.12.2017 21:49:26 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
14.12.2017 21:49:26 - "Test Case 'Version consistency' started"
14.12.2017 21:49:26 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
14.12.2017 21:49:26 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
14.12.2017 21:49:26 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
14.12.2017 21:49:26 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
14.12.2017 21:49:26 - "Test Case 'Temporal consistency' started"
14.12.2017 21:49:26 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
14.12.2017 21:49:26 - "Test Case 'Temporal consistency' finished: PASSED"
14.12.2017 21:49:26 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
14.12.2017 21:49:27 - Releasing resources
14.12.2017 21:49:27 - TestRunTask initialized
14.12.2017 21:49:27 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:27 - Validation ended with 0 error(s)
14.12.2017 21:49:27 - Compiling test script
14.12.2017 21:49:27 - Starting XQuery tests
14.12.2017 21:49:27 - "Testing 1 features"
14.12.2017 21:49:27 - "Indexing features (parsing errors: 0): 35 ms"
14.12.2017 21:49:27 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
14.12.2017 21:49:27 - "Statistics table: 0 ms"
14.12.2017 21:49:27 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
14.12.2017 21:49:27 - "Test Case 'Spatial consistency' started"
14.12.2017 21:49:27 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 0 ms"
14.12.2017 21:49:27 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Case 'Thematic consistency' started"
14.12.2017 21:49:27 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Case 'Identifiers' started"
14.12.2017 21:49:27 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
14.12.2017 21:49:27 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
14.12.2017 21:49:28 - Releasing resources
14.12.2017 21:49:28 - TestRunTask initialized
14.12.2017 21:49:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:28 - Validation ended with 0 error(s)
14.12.2017 21:49:28 - Compiling test script
14.12.2017 21:49:28 - Starting XQuery tests
14.12.2017 21:49:28 - "Testing 1 features"
14.12.2017 21:49:28 - "Indexing features (parsing errors: 0): 52 ms"
14.12.2017 21:49:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
14.12.2017 21:49:28 - "Statistics table: 0 ms"
14.12.2017 21:49:28 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
14.12.2017 21:49:28 - "Test Case 'Coordinate reference systems (CRS)' started"
14.12.2017 21:49:28 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
14.12.2017 21:49:28 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
14.12.2017 21:49:28 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
14.12.2017 21:49:29 - Releasing resources
14.12.2017 21:49:29 - TestRunTask initialized
14.12.2017 21:49:29 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:29 - Validation ended with 0 error(s)
14.12.2017 21:49:29 - Compiling test script
14.12.2017 21:49:29 - Starting XQuery tests
14.12.2017 21:49:29 - "Testing 1 features"
14.12.2017 21:49:29 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
14.12.2017 21:49:29 - "Statistics table: 0 ms"
14.12.2017 21:49:29 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
14.12.2017 21:49:29 - "Test Case 'Code lists' started"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Case 'Code lists' finished: PASSED"
14.12.2017 21:49:29 - "Test Case 'Feature references' started"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
14.12.2017 21:49:29 - "Test Case 'Feature references' finished: PASSED"
14.12.2017 21:49:29 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
14.12.2017 21:49:30 - Releasing resources
14.12.2017 21:49:30 - TestRunTask initialized
14.12.2017 21:49:30 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:30 - Validation ended with 0 error(s)
14.12.2017 21:49:30 - Compiling test script
14.12.2017 21:49:30 - Starting XQuery tests
14.12.2017 21:49:30 - "Testing 1 features"
14.12.2017 21:49:30 - "Indexing features (parsing errors: 0): 34 ms"
14.12.2017 21:49:30 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
14.12.2017 21:49:30 - "Statistics table: 1 ms"
14.12.2017 21:49:30 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
14.12.2017 21:49:30 - "Test Case 'Spatial reference systems' started"
14.12.2017 21:49:30 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
14.12.2017 21:49:30 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
14.12.2017 21:49:30 - "Test Case 'Spatial reference systems' finished: FAILED"
14.12.2017 21:49:30 - "Test Case 'Temporal reference systems' started"
14.12.2017 21:49:30 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
14.12.2017 21:49:30 - "Test Case 'Temporal reference systems' finished: PASSED"
14.12.2017 21:49:30 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
14.12.2017 21:49:31 - Releasing resources
14.12.2017 21:49:31 - TestRunTask initialized
14.12.2017 21:49:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
14.12.2017 21:49:31 - Validation ended with 0 error(s)
14.12.2017 21:49:31 - Compiling test script
14.12.2017 21:49:31 - Starting XQuery tests
14.12.2017 21:49:31 - "Testing 1 features"
14.12.2017 21:49:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
14.12.2017 21:49:31 - "Statistics table: 0 ms"
14.12.2017 21:49:31 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
14.12.2017 21:49:31 - "Test Case 'Units of measure' started"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
14.12.2017 21:49:31 - "Test Case 'Units of measure' finished: PASSED"
14.12.2017 21:49:31 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
14.12.2017 21:49:31 - Releasing resources
14.12.2017 21:49:31 - Changed state from INITIALIZED to RUNNING
14.12.2017 21:49:31 - Duration: 20sec
14.12.2017 21:49:31 - TestRun finished
14.12.2017 21:49:31 - Changed state from RUNNING to COMPLETED
