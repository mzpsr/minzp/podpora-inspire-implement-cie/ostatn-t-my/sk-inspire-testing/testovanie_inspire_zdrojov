31.01.2018 22:57:04 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava_tn-ro:RoadServiceType (initiated Wed Jan 31 22:57:04 CET 2018)
31.01.2018 22:57:04 - Resolving Executable Test Suite dependencies
31.01.2018 22:57:04 - Preparing 11 Test Task:
31.01.2018 22:57:04 -  TestTask 1 (5af522be-1cf1-4040-8066-f932a2762ed3)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 2 (ad0e4e92-e180-4025-91f3-20a74660c48c)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 3 (dd87ba59-40a5-4574-a13b-88bd82f92212)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.9af1c865-1cf0-43ff-9250-069df01b0948'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 4 (f2218c9d-377c-4c0c-afca-d0107a5db84a)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 5 (1451ba18-7f69-4b3f-a664-5e650f113332)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Road Transport Networks (EID: 14986e54-74c4-43b0-979b-d0d3e5cd0e8c, V: 0.2.1 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 6 (09660c05-bd78-42f0-9980-5db6a65fa034)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 7 (c4d13582-97ce-4b37-98bd-7e16ede456e6)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 8 (f1a67e8b-821a-43e1-ba30-35163860cac3)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 9 (856b1bc5-d09f-4e13-bc7d-6a8f1cd19811)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.2 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 10 (bf46f2c5-8284-4122-af38-7639442199cf)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 -  TestTask 11 (6696c409-49ca-44db-84ab-231d06a45c54)
31.01.2018 22:57:04 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
31.01.2018 22:57:04 -  with parameters: 
31.01.2018 22:57:04 - etf.testcases = *
31.01.2018 22:57:04 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 22:57:04 - Setting state to CREATED
31.01.2018 22:57:04 - Changed state from CREATED to INITIALIZING
