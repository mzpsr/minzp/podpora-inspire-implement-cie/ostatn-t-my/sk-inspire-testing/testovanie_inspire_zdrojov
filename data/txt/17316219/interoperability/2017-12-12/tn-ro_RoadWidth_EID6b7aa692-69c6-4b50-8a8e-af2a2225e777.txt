12.12.2017 03:04:14 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_tn-ro:RoadWidth (initiated Tue Dec 12 03:04:14 CET 2017)
12.12.2017 03:04:14 - Resolving Executable Test Suite dependencies
12.12.2017 03:04:14 - Preparing 11 Test Task:
12.12.2017 03:04:14 -  TestTask 1 (712c53a1-e47c-4f89-a6fe-1effbc55bc88)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 2 (194d447d-887d-4ac9-bf75-480c8bea2715)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 3 (6e5c7050-5f3b-49c9-9cc4-05289f567750)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.9af1c865-1cf0-43ff-9250-069df01b0948'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 4 (b64240c3-14e7-4a53-97a7-bbe6b8aa3885)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 5 (5f8db023-661a-4b62-940c-82bd29a6dc32)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Road Transport Networks (EID: 14986e54-74c4-43b0-979b-d0d3e5cd0e8c, V: 0.2.1 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 6 (1646b2c7-7d77-47b0-b44d-fad4b68fe1a3)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 7 (920285cb-ab65-4b54-9248-3b302e4a9db2)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 8 (df95f288-f1d9-44a6-9dd3-78f8b5b7bcf4)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 9 (943e7239-af35-412a-b179-316664b5620d)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.1 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 10 (ae9ebe75-b634-4574-a9c7-0625e60b1913)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 -  TestTask 11 (67bedc91-f4f9-4b7d-99ea-e264a9a4e334)
12.12.2017 03:04:14 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
12.12.2017 03:04:14 -  with parameters: 
12.12.2017 03:04:14 - etf.testcases = *
12.12.2017 03:04:14 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 03:04:14 - Setting state to CREATED
12.12.2017 03:04:14 - Changed state from CREATED to INITIALIZING
12.12.2017 03:04:14 - Starting TestRun.6b7aa692-69c6-4b50-8a8e-af2a2225e777 at 2017-12-12T03:04:16+01:00
12.12.2017 03:04:16 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 03:04:16 - TestRunTask initialized
12.12.2017 03:04:16 - Creating new tests databases to speed up tests.
12.12.2017 03:04:16 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:16 - Optimizing last database etf-tdb-2510c772-c38e-4898-836f-60d24ccea752-0 
12.12.2017 03:04:16 - Import completed
12.12.2017 03:04:18 - Validation ended with 0 error(s)
12.12.2017 03:04:18 - Compiling test script
12.12.2017 03:04:18 - Starting XQuery tests
12.12.2017 03:04:18 - "Testing 1 features"
12.12.2017 03:04:18 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
12.12.2017 03:04:18 - "Statistics table: 1 ms"
12.12.2017 03:04:18 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
12.12.2017 03:04:18 - "Test Case 'Basic tests' started"
12.12.2017 03:04:18 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
12.12.2017 03:04:18 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
12.12.2017 03:04:18 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
12.12.2017 03:04:18 - "Test Case 'Basic tests' finished: PASSED"
12.12.2017 03:04:18 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
12.12.2017 03:04:18 - Releasing resources
12.12.2017 03:04:18 - TestRunTask initialized
12.12.2017 03:04:18 - Recreating new tests databases as the Test Object has changed!
12.12.2017 03:04:18 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:18 - Optimizing last database etf-tdb-2510c772-c38e-4898-836f-60d24ccea752-0 
12.12.2017 03:04:18 - Import completed
12.12.2017 03:04:19 - Validation ended with 0 error(s)
12.12.2017 03:04:19 - Compiling test script
12.12.2017 03:04:19 - Starting XQuery tests
12.12.2017 03:04:19 - "Testing 1 features"
12.12.2017 03:04:19 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:19 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
12.12.2017 03:04:19 - "Statistics table: 1 ms"
12.12.2017 03:04:19 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
12.12.2017 03:04:19 - "Test Case 'Schema' started"
12.12.2017 03:04:19 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
12.12.2017 03:04:19 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
12.12.2017 03:04:19 - "Test Case 'Schema' finished: PASSED_MANUAL"
12.12.2017 03:04:19 - "Test Case 'Schema validation' started"
12.12.2017 03:04:19 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
12.12.2017 03:04:19 - "Validating get.xml"
12.12.2017 03:04:25 - "Duration: 6436 ms. Errors: 0."
12.12.2017 03:04:25 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 6437 ms"
12.12.2017 03:04:25 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 03:04:25 - "Test Case 'GML model' started"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Case 'GML model' finished: PASSED"
12.12.2017 03:04:25 - "Test Case 'Simple features' started"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 1 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 1 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 21 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Case 'Simple features' finished: PASSED"
12.12.2017 03:04:25 - "Test Case 'Code list values in basic data types' started"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
12.12.2017 03:04:25 - "Test Case 'Code list values in basic data types' finished: PASSED"
12.12.2017 03:04:25 - "Test Case 'Constraints' started"
12.12.2017 03:04:25 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 1 ms"
12.12.2017 03:04:25 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 03:04:25 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
12.12.2017 03:04:25 - Releasing resources
12.12.2017 03:04:25 - TestRunTask initialized
12.12.2017 03:04:25 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:25 - Validation ended with 0 error(s)
12.12.2017 03:04:25 - Compiling test script
12.12.2017 03:04:25 - Starting XQuery tests
12.12.2017 03:04:25 - "Testing 1 features"
12.12.2017 03:04:25 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-gml/ets-tn-gml-bsxets.xml"
12.12.2017 03:04:25 - "Statistics table: 0 ms"
12.12.2017 03:04:25 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' started"
12.12.2017 03:04:25 - "Test Case 'Basic test' started"
12.12.2017 03:04:25 - "Test Assertion 'tn-gml.a.1: Transport Network feature in dataset': PASSED - 0 ms"
12.12.2017 03:04:25 - "Test Case 'Basic test' finished: PASSED"
12.12.2017 03:04:25 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' finished: PASSED"
12.12.2017 03:04:26 - Releasing resources
12.12.2017 03:04:26 - TestRunTask initialized
12.12.2017 03:04:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:26 - Validation ended with 0 error(s)
12.12.2017 03:04:26 - Compiling test script
12.12.2017 03:04:26 - Starting XQuery tests
12.12.2017 03:04:26 - "Testing 1 features"
12.12.2017 03:04:26 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:26 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-as/ets-tn-as-bsxets.xml"
12.12.2017 03:04:26 - "Statistics table: 0 ms"
12.12.2017 03:04:26 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' started"
12.12.2017 03:04:26 - "Test Case 'Code list values' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.a.1: AccessRestrictionValue attributes': PASSED - 4 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.a.2: RestrictionTypeValue attributes': PASSED - 4 ms"
12.12.2017 03:04:26 - "Test Case 'Code list values' finished: PASSED"
12.12.2017 03:04:26 - "Test Case 'Constraints' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.1: TrafficFlowDirection can only be associated with a spatial object of the type Link or LinkSequence.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.2: All transport areas have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.3: All transport links have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.4: All transport link sequences have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.5: All transport link sets have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.6: All transport nodes have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.7: All transport points have an external object identifier.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.8: All transport properties have an external object identifier.': PASSED - 1 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.9: A transport link sequence must be composed of transport links that all belong to the same transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.b.10: A transport link set must be composed of transport links and or transport link sequences that all belong to the same transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 03:04:26 - "Test Case 'Geometry' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.c.1: No free transport nodes': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.c.2: Intersections only at crossings': PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Case 'Geometry' finished: PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Case 'Network connectivity' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.d.1: Connectivity at crossings': PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.d.2: Unconnected nodes': PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.d.3: Connectivity tolerance': PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Case 'Network connectivity' finished: PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Case 'Object References' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.e.1: Linear references': PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Assertion 'tn-as.e.2: Inter-modal connections': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Case 'Object References' finished: PASSED_MANUAL"
12.12.2017 03:04:26 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' finished: PASSED_MANUAL"
12.12.2017 03:04:26 - Releasing resources
12.12.2017 03:04:26 - TestRunTask initialized
12.12.2017 03:04:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:26 - Validation ended with 0 error(s)
12.12.2017 03:04:26 - Compiling test script
12.12.2017 03:04:26 - Starting XQuery tests
12.12.2017 03:04:26 - "Testing 1 features"
12.12.2017 03:04:26 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ro-as/ets-tn-ro-as-bsxets.xml"
12.12.2017 03:04:26 - "Statistics table: 0 ms"
12.12.2017 03:04:26 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' started"
12.12.2017 03:04:26 - "Test Case 'Code list values' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.1: AreaConditionValue attributes': PASSED - 5 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.2: FormOfRoadNodeValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.3: FormOfWayValue attributes': PASSED - 4 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.4: RoadPartValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.5: RoadServiceTypeValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.6: RoadSurfaceCategoryValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.7: ServiceFacilityValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.8: SpeedLimitSourceValue attributes': PASSED - 4 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.9: VehicleTypeValue attributes': PASSED - 6 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.a.10: WeatherConditionValue attributes': PASSED - 3 ms"
12.12.2017 03:04:26 - "Test Case 'Code list values' finished: PASSED"
12.12.2017 03:04:26 - "Test Case 'Constraints' started"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.b.1: FormOfWay can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.b.2: FunctionalRoadClass can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.b.3: NumberOfLanes can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.b.4: RoadName can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Test Assertion 'tn-ro-as.b.5: RoadSurfaceCategory can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:26 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=netElementL.1400000'"
12.12.2017 03:04:56 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=netElementL.1400000"
12.12.2017 03:04:56 - "Test Assertion 'tn-ro-as.b.6: RoadWidth can only be associated with a spatial object that is part of a road transport network.': FAILED - 30010 ms"
12.12.2017 03:04:56 - "Test Assertion 'tn-ro-as.b.7: SpeedLimit can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
12.12.2017 03:04:56 - "Test Assertion 'tn-ro-as.b.8: RoadServiceType can only be associated with a spatial object of the type RoadServiceArea or RoadNode (when formOfRoadNode=roadServiceArea)': PASSED - 0 ms"
12.12.2017 03:04:56 - "Test Case 'Constraints' finished: FAILED"
12.12.2017 03:04:56 - "Test Case 'Link centrelines' started"
12.12.2017 03:04:56 - "Test Assertion 'tn-ro-as.c.1: Link centrelines test': PASSED_MANUAL"
12.12.2017 03:04:56 - "Test Case 'Link centrelines' finished: PASSED_MANUAL"
12.12.2017 03:04:56 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' finished: FAILED"
12.12.2017 03:04:56 - Releasing resources
12.12.2017 03:04:56 - TestRunTask initialized
12.12.2017 03:04:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:56 - Validation ended with 0 error(s)
12.12.2017 03:04:56 - Compiling test script
12.12.2017 03:04:57 - Starting XQuery tests
12.12.2017 03:04:57 - "Testing 1 features"
12.12.2017 03:04:57 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
12.12.2017 03:04:57 - "Statistics table: 0 ms"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
12.12.2017 03:04:57 - "Test Case 'Version consistency' started"
12.12.2017 03:04:57 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
12.12.2017 03:04:57 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
12.12.2017 03:04:57 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
12.12.2017 03:04:57 - "Test Case 'Temporal consistency' started"
12.12.2017 03:04:57 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Case 'Temporal consistency' finished: PASSED"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
12.12.2017 03:04:57 - Releasing resources
12.12.2017 03:04:57 - TestRunTask initialized
12.12.2017 03:04:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:57 - Validation ended with 0 error(s)
12.12.2017 03:04:57 - Compiling test script
12.12.2017 03:04:57 - Starting XQuery tests
12.12.2017 03:04:57 - "Testing 1 features"
12.12.2017 03:04:57 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-dc/ets-tn-dc-bsxets.xml"
12.12.2017 03:04:57 - "Statistics table: 0 ms"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Data consistency, Transport Networks' started"
12.12.2017 03:04:57 - "Test Case 'Spatial consistency' started"
12.12.2017 03:04:57 - "Test Assertion 'tn-dc.a.1: Each Transport Network Link or Node geometry is within a Network Area geometry (Road Transport Networks)': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Assertion 'tn-dc.a.2: Each Transport Network Link or Node geometry is within a Network Area geometry (Railway Transport Networks)': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Assertion 'tn-dc.a.3: Each Transport Network Link or Node geometry is within a Network Area geometry (Waterway Transport Networks)': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Assertion 'tn-dc.a.4: Each Transport Network Link or Node geometry is within a Network Area geometry (Air Transport Networks)': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Assertion 'tn-dc.a.5: Manual review': PASSED_MANUAL"
12.12.2017 03:04:57 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Data consistency, Transport Networks' finished: PASSED_MANUAL"
12.12.2017 03:04:57 - Releasing resources
12.12.2017 03:04:57 - TestRunTask initialized
12.12.2017 03:04:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:57 - Validation ended with 0 error(s)
12.12.2017 03:04:57 - Compiling test script
12.12.2017 03:04:57 - Starting XQuery tests
12.12.2017 03:04:57 - "Testing 1 features"
12.12.2017 03:04:57 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
12.12.2017 03:04:57 - "Statistics table: 0 ms"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
12.12.2017 03:04:57 - "Test Case 'Coordinate reference systems (CRS)' started"
12.12.2017 03:04:57 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Case 'Coordinate reference systems (CRS)' finished: PASSED"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: PASSED"
12.12.2017 03:04:57 - Releasing resources
12.12.2017 03:04:57 - TestRunTask initialized
12.12.2017 03:04:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:57 - Validation ended with 0 error(s)
12.12.2017 03:04:57 - Compiling test script
12.12.2017 03:04:57 - Starting XQuery tests
12.12.2017 03:04:57 - "Testing 1 features"
12.12.2017 03:04:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ia/ets-tn-ia-bsxets.xml"
12.12.2017 03:04:57 - "Statistics table: 1 ms"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' started"
12.12.2017 03:04:57 - "Test Case 'Code lists' started"
12.12.2017 03:04:57 - "Test Assertion 'tn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Case 'Code lists' finished: PASSED"
12.12.2017 03:04:57 - "Test Case 'Feature references' started"
12.12.2017 03:04:57 - "Test Assertion 'tn-ia.b.1: MarkerPost.route': PASSED - 1 ms"
12.12.2017 03:04:57 - "Test Assertion 'tn-ia.b.2: TransportLinkSet.post': PASSED - 0 ms"
12.12.2017 03:04:57 - "Test Case 'Feature references' finished: PASSED"
12.12.2017 03:04:57 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' finished: PASSED"
12.12.2017 03:04:57 - Releasing resources
12.12.2017 03:04:57 - TestRunTask initialized
12.12.2017 03:04:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:57 - Validation ended with 0 error(s)
12.12.2017 03:04:57 - Compiling test script
12.12.2017 03:04:57 - Starting XQuery tests
12.12.2017 03:04:58 - "Testing 1 features"
12.12.2017 03:04:58 - "Indexing features (parsing errors: 0): 0 ms"
12.12.2017 03:04:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
12.12.2017 03:04:58 - "Statistics table: 1 ms"
12.12.2017 03:04:58 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
12.12.2017 03:04:58 - "Test Case 'Spatial reference systems' started"
12.12.2017 03:04:58 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': PASSED - 0 ms"
12.12.2017 03:04:58 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
12.12.2017 03:04:58 - "Test Case 'Spatial reference systems' finished: PASSED"
12.12.2017 03:04:58 - "Test Case 'Temporal reference systems' started"
12.12.2017 03:04:58 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
12.12.2017 03:04:58 - "Test Case 'Temporal reference systems' finished: PASSED"
12.12.2017 03:04:58 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: PASSED"
12.12.2017 03:04:58 - Releasing resources
12.12.2017 03:04:58 - TestRunTask initialized
12.12.2017 03:04:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 03:04:58 - Validation ended with 0 error(s)
12.12.2017 03:04:58 - Compiling test script
12.12.2017 03:04:58 - Starting XQuery tests
12.12.2017 03:04:58 - "Testing 1 features"
12.12.2017 03:04:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-rs/ets-tn-rs-bsxets.xml"
12.12.2017 03:04:58 - "Statistics table: 0 ms"
12.12.2017 03:04:58 - "Test Suite 'Conformance class: Reference systems, Transport Networks' started"
12.12.2017 03:04:58 - "Test Case 'Additional theme-specific rules for reference systems' started"
12.12.2017 03:04:58 - "Test Assertion 'tn-rs.a.1: Test always passes': PASSED - 0 ms"
12.12.2017 03:04:58 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
12.12.2017 03:04:58 - "Test Suite 'Conformance class: Reference systems, Transport Networks' finished: PASSED"
12.12.2017 03:04:58 - Releasing resources
12.12.2017 03:04:58 - Changed state from INITIALIZED to RUNNING
12.12.2017 03:04:58 - Duration: 44sec
12.12.2017 03:04:58 - TestRun finished
12.12.2017 03:04:58 - Changed state from RUNNING to COMPLETED
