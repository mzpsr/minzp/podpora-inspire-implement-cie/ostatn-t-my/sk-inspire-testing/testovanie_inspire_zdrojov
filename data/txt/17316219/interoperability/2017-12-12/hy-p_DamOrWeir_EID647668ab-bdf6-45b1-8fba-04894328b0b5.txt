12.12.2017 02:53:22 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_hy-p:DamOrWeir (initiated Tue Dec 12 02:53:22 CET 2017)
12.12.2017 02:53:22 - Resolving Executable Test Suite dependencies
12.12.2017 02:53:22 - Preparing 10 Test Task:
12.12.2017 02:53:22 -  TestTask 1 (c2b0294b-57a2-4bbe-be4a-7d2ce9c56e53)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 2 (2a0ac63a-d0ef-4e5a-a2c9-a56413a7c4e5)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 3 (ecaf06cf-c129-4d0b-97ff-cda256f73b12)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 4 (56de565a-a7d9-4360-9b16-667c29cf890e)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 5 (760466ca-0d02-41ad-85c1-f08aa652db81)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 6 (742f049a-3515-4e5d-b23c-e157be373623)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 7 (d3161e0d-4834-4036-b0f9-a6646e32e804)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 8 (2ec4cd82-70c9-4e5d-a5b3-f76babf41cfa)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 9 (fbbaee61-5e3c-4d88-acff-d4041a1e2804)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 -  TestTask 10 (35668c3d-e2d9-41af-8013-ae42c455568b)
12.12.2017 02:53:22 -  will perform tests on Test Object 'get.html' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
12.12.2017 02:53:22 -  with parameters: 
12.12.2017 02:53:22 - etf.testcases = *
12.12.2017 02:53:22 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 02:53:22 - Setting state to CREATED
12.12.2017 02:53:22 - Changed state from CREATED to INITIALIZING
12.12.2017 02:53:22 - Starting TestRun.647668ab-bdf6-45b1-8fba-04894328b0b5 at 2017-12-12T02:53:23+01:00
12.12.2017 02:53:23 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 02:53:23 - TestRunTask initialized
12.12.2017 02:53:23 - Creating new tests databases to speed up tests.
12.12.2017 02:53:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:23 - Optimizing last database etf-tdb-941e4f1c-159e-4573-b28f-ee6ef164004a-0 
12.12.2017 02:53:23 - Import completed
12.12.2017 02:53:24 - Validation ended with 0 error(s)
12.12.2017 02:53:24 - Compiling test script
12.12.2017 02:53:24 - Starting XQuery tests
12.12.2017 02:53:24 - "Testing 1 features"
12.12.2017 02:53:24 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
12.12.2017 02:53:24 - "Statistics table: 0 ms"
12.12.2017 02:53:24 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
12.12.2017 02:53:24 - "Test Case 'Basic tests' started"
12.12.2017 02:53:24 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
12.12.2017 02:53:24 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
12.12.2017 02:53:24 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
12.12.2017 02:53:24 - "Test Case 'Basic tests' finished: PASSED"
12.12.2017 02:53:24 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
12.12.2017 02:53:25 - Releasing resources
12.12.2017 02:53:25 - TestRunTask initialized
12.12.2017 02:53:25 - Recreating new tests databases as the Test Object has changed!
12.12.2017 02:53:25 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:25 - Optimizing last database etf-tdb-941e4f1c-159e-4573-b28f-ee6ef164004a-0 
12.12.2017 02:53:25 - Import completed
12.12.2017 02:53:25 - Validation ended with 0 error(s)
12.12.2017 02:53:25 - Compiling test script
12.12.2017 02:53:25 - Starting XQuery tests
12.12.2017 02:53:25 - "Testing 1 features"
12.12.2017 02:53:25 - "Indexing features (parsing errors: 0): 38 ms"
12.12.2017 02:53:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
12.12.2017 02:53:25 - "Statistics table: 1 ms"
12.12.2017 02:53:25 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
12.12.2017 02:53:25 - "Test Case 'Schema' started"
12.12.2017 02:53:25 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
12.12.2017 02:53:25 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
12.12.2017 02:53:25 - "Test Case 'Schema' finished: PASSED_MANUAL"
12.12.2017 02:53:25 - "Test Case 'Schema validation' started"
12.12.2017 02:53:25 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
12.12.2017 02:53:25 - "Validating get.xml"
12.12.2017 02:53:31 - "Duration: 6107 ms. Errors: 0."
12.12.2017 02:53:31 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 6108 ms"
12.12.2017 02:53:31 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 02:53:31 - "Test Case 'GML model' started"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Case 'GML model' finished: PASSED"
12.12.2017 02:53:31 - "Test Case 'Simple features' started"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 1 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 1 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 37 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Case 'Simple features' finished: PASSED"
12.12.2017 02:53:31 - "Test Case 'Code list values in basic data types' started"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
12.12.2017 02:53:31 - "Test Case 'Code list values in basic data types' finished: PASSED"
12.12.2017 02:53:31 - "Test Case 'Constraints' started"
12.12.2017 02:53:31 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Case 'Constraints' finished: PASSED"
12.12.2017 02:53:31 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
12.12.2017 02:53:31 - Releasing resources
12.12.2017 02:53:31 - TestRunTask initialized
12.12.2017 02:53:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:31 - Validation ended with 0 error(s)
12.12.2017 02:53:31 - Compiling test script
12.12.2017 02:53:31 - Starting XQuery tests
12.12.2017 02:53:31 - "Testing 1 features"
12.12.2017 02:53:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
12.12.2017 02:53:31 - "Statistics table: 0 ms"
12.12.2017 02:53:31 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
12.12.2017 02:53:31 - "Test Case 'Basic test' started"
12.12.2017 02:53:31 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
12.12.2017 02:53:31 - "Test Case 'Basic test' finished: PASSED"
12.12.2017 02:53:31 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
12.12.2017 02:53:31 - Releasing resources
12.12.2017 02:53:31 - TestRunTask initialized
12.12.2017 02:53:31 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:31 - Validation ended with 0 error(s)
12.12.2017 02:53:31 - Compiling test script
12.12.2017 02:53:31 - Starting XQuery tests
12.12.2017 02:53:32 - "Testing 1 features"
12.12.2017 02:53:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
12.12.2017 02:53:32 - "Statistics table: 1 ms"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
12.12.2017 02:53:32 - "Test Case 'Code list values' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 5 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 3 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 7 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 3 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 3 ms"
12.12.2017 02:53:32 - "Test Case 'Code list values' finished: PASSED"
12.12.2017 02:53:32 - "Test Case 'Geometry' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Geometry' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Identifiers and references' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Constraints' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Constraints' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - Releasing resources
12.12.2017 02:53:32 - TestRunTask initialized
12.12.2017 02:53:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:32 - Validation ended with 0 error(s)
12.12.2017 02:53:32 - Compiling test script
12.12.2017 02:53:32 - Starting XQuery tests
12.12.2017 02:53:32 - "Testing 1 features"
12.12.2017 02:53:32 - "Indexing features (parsing errors: 0): 39 ms"
12.12.2017 02:53:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
12.12.2017 02:53:32 - "Statistics table: 0 ms"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
12.12.2017 02:53:32 - "Test Case 'Version consistency' started"
12.12.2017 02:53:32 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Temporal consistency' started"
12.12.2017 02:53:32 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Case 'Temporal consistency' finished: PASSED"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - Releasing resources
12.12.2017 02:53:32 - TestRunTask initialized
12.12.2017 02:53:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:32 - Validation ended with 0 error(s)
12.12.2017 02:53:32 - Compiling test script
12.12.2017 02:53:32 - Starting XQuery tests
12.12.2017 02:53:32 - "Testing 1 features"
12.12.2017 02:53:32 - "Indexing features (parsing errors: 0): 35 ms"
12.12.2017 02:53:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
12.12.2017 02:53:32 - "Statistics table: 0 ms"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
12.12.2017 02:53:32 - "Test Case 'Spatial consistency' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 0 ms"
12.12.2017 02:53:32 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Thematic consistency' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Identifiers' started"
12.12.2017 02:53:32 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
12.12.2017 02:53:32 - Releasing resources
12.12.2017 02:53:32 - TestRunTask initialized
12.12.2017 02:53:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:32 - Validation ended with 0 error(s)
12.12.2017 02:53:32 - Compiling test script
12.12.2017 02:53:32 - Starting XQuery tests
12.12.2017 02:53:32 - "Testing 1 features"
12.12.2017 02:53:32 - "Indexing features (parsing errors: 0): 38 ms"
12.12.2017 02:53:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
12.12.2017 02:53:32 - "Statistics table: 0 ms"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
12.12.2017 02:53:32 - "Test Case 'Coordinate reference systems (CRS)' started"
12.12.2017 02:53:32 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
12.12.2017 02:53:32 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
12.12.2017 02:53:32 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
12.12.2017 02:53:33 - Releasing resources
12.12.2017 02:53:33 - TestRunTask initialized
12.12.2017 02:53:33 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:33 - Validation ended with 0 error(s)
12.12.2017 02:53:33 - Compiling test script
12.12.2017 02:53:33 - Starting XQuery tests
12.12.2017 02:53:33 - "Testing 1 features"
12.12.2017 02:53:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
12.12.2017 02:53:33 - "Statistics table: 1 ms"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
12.12.2017 02:53:33 - "Test Case 'Code lists' started"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Case 'Code lists' finished: PASSED"
12.12.2017 02:53:33 - "Test Case 'Feature references' started"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 1 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Case 'Feature references' finished: PASSED"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
12.12.2017 02:53:33 - Releasing resources
12.12.2017 02:53:33 - TestRunTask initialized
12.12.2017 02:53:33 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:33 - Validation ended with 0 error(s)
12.12.2017 02:53:33 - Compiling test script
12.12.2017 02:53:33 - Starting XQuery tests
12.12.2017 02:53:33 - "Testing 1 features"
12.12.2017 02:53:33 - "Indexing features (parsing errors: 0): 36 ms"
12.12.2017 02:53:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
12.12.2017 02:53:33 - "Statistics table: 0 ms"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
12.12.2017 02:53:33 - "Test Case 'Spatial reference systems' started"
12.12.2017 02:53:33 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Case 'Spatial reference systems' finished: FAILED"
12.12.2017 02:53:33 - "Test Case 'Temporal reference systems' started"
12.12.2017 02:53:33 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Case 'Temporal reference systems' finished: PASSED"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
12.12.2017 02:53:33 - Releasing resources
12.12.2017 02:53:33 - TestRunTask initialized
12.12.2017 02:53:33 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:53:33 - Validation ended with 0 error(s)
12.12.2017 02:53:33 - Compiling test script
12.12.2017 02:53:33 - Starting XQuery tests
12.12.2017 02:53:33 - "Testing 1 features"
12.12.2017 02:53:33 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
12.12.2017 02:53:33 - "Statistics table: 0 ms"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
12.12.2017 02:53:33 - "Test Case 'Units of measure' started"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
12.12.2017 02:53:33 - "Test Case 'Units of measure' finished: PASSED"
12.12.2017 02:53:33 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
12.12.2017 02:53:34 - Releasing resources
12.12.2017 02:53:34 - Changed state from INITIALIZED to RUNNING
12.12.2017 02:53:34 - Duration: 11sec
12.12.2017 02:53:34 - TestRun finished
12.12.2017 02:53:34 - Changed state from RUNNING to COMPLETED
