29.11.2017 21:48:18 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava (initiated Wed Nov 29 21:48:18 CET 2017)
29.11.2017 21:48:18 - Resolving Executable Test Suite dependencies
29.11.2017 21:48:18 - Preparing 2 Test Task:
29.11.2017 21:48:18 -  TestTask 1 (1555ba8c-2f43-4c2b-8fd8-6ed21ab8caee)
29.11.2017 21:48:18 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
29.11.2017 21:48:18 -  with parameters: 
29.11.2017 21:48:18 - etf.testcases = *
29.11.2017 21:48:18 -  TestTask 2 (6a739f24-1ad6-42fd-a0ac-c62b950016ca)
29.11.2017 21:48:18 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
29.11.2017 21:48:18 -  with parameters: 
29.11.2017 21:48:18 - etf.testcases = *
29.11.2017 21:48:18 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
29.11.2017 21:48:18 - Setting state to CREATED
29.11.2017 21:48:18 - Changed state from CREATED to INITIALIZING
29.11.2017 21:48:19 - Starting TestRun.c5a51061-193e-4622-893c-b47aa84f68c6 at 2017-11-29T21:48:20+01:00
29.11.2017 21:48:20 - Changed state from INITIALIZING to INITIALIZED
29.11.2017 21:48:20 - TestRunTask initialized
29.11.2017 21:48:20 - Creating new tests databases to speed up tests.
29.11.2017 21:48:20 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
29.11.2017 21:48:20 - Optimizing last database etf-tdb-d130c781-b96a-45d8-ac9d-3be512a00062-0 
29.11.2017 21:48:20 - Import completed
29.11.2017 21:48:20 - Validation ended with 0 error(s)
29.11.2017 21:48:20 - Compiling test script
29.11.2017 21:48:20 - Starting XQuery tests
29.11.2017 21:48:20 - "Testing 1 records"
29.11.2017 21:48:20 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
29.11.2017 21:48:20 - "Statistics table: 0 ms"
29.11.2017 21:48:20 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
29.11.2017 21:48:20 - "Test Case 'Schema validation' started"
29.11.2017 21:48:22 - "Validating file GetRecordByIdResponse.xml: 1961 ms"
29.11.2017 21:48:22 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1961 ms"
29.11.2017 21:48:22 - "Test Case 'Schema validation' finished: PASSED"
29.11.2017 21:48:22 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
29.11.2017 21:48:23 - Releasing resources
29.11.2017 21:48:23 - TestRunTask initialized
29.11.2017 21:48:23 - Recreating new tests databases as the Test Object has changed!
29.11.2017 21:48:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
29.11.2017 21:48:23 - Optimizing last database etf-tdb-d130c781-b96a-45d8-ac9d-3be512a00062-0 
29.11.2017 21:48:23 - Import completed
29.11.2017 21:48:23 - Validation ended with 0 error(s)
29.11.2017 21:48:23 - Compiling test script
29.11.2017 21:48:23 - Starting XQuery tests
29.11.2017 21:48:23 - "Testing 1 records"
29.11.2017 21:48:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
29.11.2017 21:48:23 - "Statistics table: 0 ms"
29.11.2017 21:48:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
29.11.2017 21:48:23 - "Test Case 'Common tests' started"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 1 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Case 'Common tests' finished: PASSED_MANUAL"
29.11.2017 21:48:23 - "Test Case 'Hierarchy level' started"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Case 'Hierarchy level' finished: PASSED"
29.11.2017 21:48:23 - "Test Case 'Dataset (series) tests' started"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
29.11.2017 21:48:23 - "Test Case 'Dataset (series) tests' finished: PASSED"
29.11.2017 21:48:23 - "Test Case 'Service tests' started"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
29.11.2017 21:48:23 - "Checking URL: 'https://zbgis.skgeodesy.sk/tkgis/?addWMS=https://zbgisws.skgeodesy.sk/inspire_hydrography_wms/service.svc/get'"
29.11.2017 21:48:23 - "Checking URL: 'https://zbgis.skgeodesy.sk/mkzbgis/'"
29.11.2017 21:48:23 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_hydrography_wms/service.svc/get'"
29.11.2017 21:48:23 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 340 ms"
29.11.2017 21:48:23 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_ZBGIS_INSPIRE_HY'"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 398 ms"
29.11.2017 21:48:24 - "Test Case 'Service tests' finished: PASSED_MANUAL"
29.11.2017 21:48:24 - "Test Case 'Keywords' started"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
29.11.2017 21:48:24 - "Test Case 'Keywords' finished: PASSED"
29.11.2017 21:48:24 - "Test Case 'Keywords - details' started"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
29.11.2017 21:48:24 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 7 ms"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 1 ms"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
29.11.2017 21:48:24 - "Test Case 'Keywords - details' finished: PASSED"
29.11.2017 21:48:24 - "Test Case 'Temporal extent' started"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
29.11.2017 21:48:24 - "Test Case 'Temporal extent' finished: PASSED"
29.11.2017 21:48:24 - "Test Case 'Temporal extent - details' started"
29.11.2017 21:48:24 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
29.11.2017 21:48:24 - "Test Case 'Temporal extent - details' finished: PASSED"
29.11.2017 21:48:24 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
29.11.2017 21:48:24 - Releasing resources
29.11.2017 21:48:24 - Changed state from INITIALIZED to RUNNING
29.11.2017 21:48:24 - Duration: 6sec
29.11.2017 21:48:24 - TestRun finished
29.11.2017 21:48:24 - Changed state from RUNNING to COMPLETED
