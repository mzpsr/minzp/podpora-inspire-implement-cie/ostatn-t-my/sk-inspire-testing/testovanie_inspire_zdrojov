05.12.2017 13:45:35 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_INSPIRE - Geografick&eacute; n&aacute;zvoslovie (initiated Tue Dec 05 13:45:35 CET 2017)
05.12.2017 13:45:35 - Resolving Executable Test Suite dependencies
05.12.2017 13:45:35 - Preparing 2 Test Task:
05.12.2017 13:45:35 -  TestTask 1 (be7d6c75-8319-49e7-a19e-75d1111bcf3e)
05.12.2017 13:45:35 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
05.12.2017 13:45:35 -  with parameters: 
05.12.2017 13:45:35 - etf.testcases = *
05.12.2017 13:45:35 -  TestTask 2 (4fb284c6-001e-498b-940b-064677aefe04)
05.12.2017 13:45:35 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
05.12.2017 13:45:35 -  with parameters: 
05.12.2017 13:45:35 - etf.testcases = *
05.12.2017 13:45:35 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
05.12.2017 13:45:35 - Setting state to CREATED
05.12.2017 13:45:35 - Changed state from CREATED to INITIALIZING
05.12.2017 13:45:41 - Starting TestRun.eb2bb23d-4f55-4baf-85a5-647058748ae8 at 2017-12-05T13:45:43+01:00
05.12.2017 13:45:43 - Changed state from INITIALIZING to INITIALIZED
05.12.2017 13:45:43 - TestRunTask initialized
05.12.2017 13:45:43 - Creating new tests databases to speed up tests.
05.12.2017 13:45:43 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 13:45:43 - Optimizing last database etf-tdb-fb44a7d5-2510-4b1e-8b32-fd958c9c393b-0 
05.12.2017 13:45:43 - Import completed
05.12.2017 13:45:43 - Validation ended with 0 error(s)
05.12.2017 13:45:43 - Compiling test script
05.12.2017 13:45:43 - Starting XQuery tests
05.12.2017 13:45:43 - "Testing 1 records"
05.12.2017 13:45:43 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
05.12.2017 13:45:43 - "Statistics table: 0 ms"
05.12.2017 13:45:43 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
05.12.2017 13:45:43 - "Test Case 'Schema validation' started"
05.12.2017 13:45:45 - "Validating file GetRecordByIdResponse.xml: 1925 ms"
05.12.2017 13:45:45 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1926 ms"
05.12.2017 13:45:45 - "Test Case 'Schema validation' finished: PASSED"
05.12.2017 13:45:45 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
05.12.2017 13:45:46 - Releasing resources
05.12.2017 13:45:46 - TestRunTask initialized
05.12.2017 13:45:46 - Recreating new tests databases as the Test Object has changed!
05.12.2017 13:45:46 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 13:45:46 - Optimizing last database etf-tdb-fb44a7d5-2510-4b1e-8b32-fd958c9c393b-0 
05.12.2017 13:45:46 - Import completed
05.12.2017 13:45:46 - Validation ended with 0 error(s)
05.12.2017 13:45:46 - Compiling test script
05.12.2017 13:45:46 - Starting XQuery tests
05.12.2017 13:45:46 - "Testing 1 records"
05.12.2017 13:45:46 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
05.12.2017 13:45:46 - "Statistics table: 0 ms"
05.12.2017 13:45:46 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
05.12.2017 13:45:46 - "Test Case 'Common tests' started"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 1 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Case 'Common tests' finished: PASSED_MANUAL"
05.12.2017 13:45:46 - "Test Case 'Hierarchy level' started"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Case 'Hierarchy level' finished: PASSED"
05.12.2017 13:45:46 - "Test Case 'Dataset (series) tests' started"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
05.12.2017 13:45:46 - "Checking URL: 'http://www.geoportal.sk'"
05.12.2017 13:45:46 - "Checking URL: 'https://www.geoportal.sk/'"
05.12.2017 13:45:46 - "Exception: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target URL: https://www.geoportal.sk/"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.3: Dataset linkage': FAILED - 262 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 1 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Case 'Dataset (series) tests' finished: FAILED"
05.12.2017 13:45:46 - "Test Case 'Service tests' started"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 1 ms"
05.12.2017 13:45:46 - "Test Case 'Service tests' finished: PASSED"
05.12.2017 13:45:46 - "Test Case 'Keywords' started"
05.12.2017 13:45:46 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
05.12.2017 13:45:46 - "Test Case 'Keywords' finished: PASSED"
05.12.2017 13:45:46 - "Test Case 'Keywords - details' started"
05.12.2017 13:45:46 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
05.12.2017 13:45:47 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 850 ms"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
05.12.2017 13:45:47 - "Test Case 'Keywords - details' finished: PASSED"
05.12.2017 13:45:47 - "Test Case 'Temporal extent' started"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
05.12.2017 13:45:47 - "Test Case 'Temporal extent' finished: PASSED"
05.12.2017 13:45:47 - "Test Case 'Temporal extent - details' started"
05.12.2017 13:45:47 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
05.12.2017 13:45:47 - "Test Case 'Temporal extent - details' finished: PASSED"
05.12.2017 13:45:47 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
05.12.2017 13:45:49 - Releasing resources
05.12.2017 13:45:49 - Changed state from INITIALIZED to RUNNING
05.12.2017 13:45:49 - Duration: 14sec
05.12.2017 13:45:49 - TestRun finished
05.12.2017 13:45:49 - Changed state from RUNNING to COMPLETED
