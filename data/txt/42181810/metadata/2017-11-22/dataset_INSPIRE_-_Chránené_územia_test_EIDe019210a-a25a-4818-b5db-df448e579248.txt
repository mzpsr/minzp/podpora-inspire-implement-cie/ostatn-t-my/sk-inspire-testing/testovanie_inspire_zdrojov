22.11.2017 00:05:24 - Preparing Test Run metadata (initiated Wed Nov 22 00:05:24 CET 2017)
22.11.2017 00:05:24 - Resolving Executable Test Suite dependencies
22.11.2017 00:05:24 - Preparing 2 Test Task:
22.11.2017 00:05:24 -  TestTask 1 (5cce49e3-f127-448a-93a7-4b45c91cd48a)
22.11.2017 00:05:24 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
22.11.2017 00:05:24 -  with parameters: 
22.11.2017 00:05:24 - etf.testcases = *
22.11.2017 00:05:24 -  TestTask 2 (46a85db8-e401-4bbb-83f2-43a3d66e03e1)
22.11.2017 00:05:24 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
22.11.2017 00:05:24 -  with parameters: 
22.11.2017 00:05:24 - etf.testcases = *
22.11.2017 00:05:24 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
22.11.2017 00:05:24 - Setting state to CREATED
22.11.2017 00:05:24 - Changed state from CREATED to INITIALIZING
22.11.2017 00:05:24 - Starting TestRun.e019210a-a25a-4818-b5db-df448e579248 at 2017-11-22T00:05:25+01:00
22.11.2017 00:05:25 - Changed state from INITIALIZING to INITIALIZED
22.11.2017 00:05:25 - TestRunTask initialized
22.11.2017 00:05:25 - Creating new tests databases to speed up tests.
22.11.2017 00:05:25 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 00:05:25 - Optimizing last database etf-tdb-8ba079a6-335e-4de4-a8b3-f28726cc8259-0 
22.11.2017 00:05:25 - Import completed
22.11.2017 00:05:25 - Validation ended with 0 error(s)
22.11.2017 00:05:25 - Compiling test script
22.11.2017 00:05:25 - Starting XQuery tests
22.11.2017 00:05:25 - "Testing 1 records"
22.11.2017 00:05:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
22.11.2017 00:05:25 - "Statistics table: 1 ms"
22.11.2017 00:05:25 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
22.11.2017 00:05:25 - "Test Case 'Schema validation' started"
22.11.2017 00:05:31 - "Validating file GetRecordByIdResponse.xml: 6005 ms"
22.11.2017 00:05:31 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 6007 ms"
22.11.2017 00:05:31 - "Test Case 'Schema validation' finished: PASSED"
22.11.2017 00:05:31 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
22.11.2017 00:05:32 - Releasing resources
22.11.2017 00:05:32 - TestRunTask initialized
22.11.2017 00:05:32 - Recreating new tests databases as the Test Object has changed!
22.11.2017 00:05:32 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.11.2017 00:05:32 - Optimizing last database etf-tdb-8ba079a6-335e-4de4-a8b3-f28726cc8259-0 
22.11.2017 00:05:32 - Import completed
22.11.2017 00:05:32 - Validation ended with 0 error(s)
22.11.2017 00:05:32 - Compiling test script
22.11.2017 00:05:32 - Starting XQuery tests
22.11.2017 00:05:32 - "Testing 1 records"
22.11.2017 00:05:32 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
22.11.2017 00:05:32 - "Statistics table: 1 ms"
22.11.2017 00:05:32 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
22.11.2017 00:05:32 - "Test Case 'Common tests' started"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.1: Title': PASSED - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Case 'Common tests' finished: PASSED_MANUAL"
22.11.2017 00:05:32 - "Test Case 'Hierarchy level' started"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
22.11.2017 00:05:32 - "Test Case 'Hierarchy level' finished: PASSED"
22.11.2017 00:05:32 - "Test Case 'Dataset (series) tests' started"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 1 ms"
22.11.2017 00:05:32 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
22.11.2017 00:05:32 - "Checking URL: 'http://inspire.biomonitoring.sk/geoserver/ows?service=WFS&amp;count=1&amp;REQUEST=GetFeature&amp;version=2.0.0&amp;namespaces=xmlns(ps-f,urn:x-inspire:specification:gmlas:ProtectedSitesFull:3.0)&amp;TYPENAMES=ps-f:ProtectedSite'"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 6076 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 1 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Case 'Dataset (series) tests' finished: PASSED_MANUAL"
22.11.2017 00:05:38 - "Test Case 'Service tests' started"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Case 'Service tests' finished: PASSED"
22.11.2017 00:05:38 - "Test Case 'Keywords' started"
22.11.2017 00:05:38 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
22.11.2017 00:05:38 - "Test Case 'Keywords' finished: PASSED"
22.11.2017 00:05:38 - "Test Case 'Keywords - details' started"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
22.11.2017 00:05:38 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
22.11.2017 00:05:39 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
22.11.2017 00:05:39 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 368 ms"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
22.11.2017 00:05:39 - "Test Case 'Keywords - details' finished: PASSED"
22.11.2017 00:05:39 - "Test Case 'Temporal extent' started"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
22.11.2017 00:05:39 - "Test Case 'Temporal extent' finished: PASSED"
22.11.2017 00:05:39 - "Test Case 'Temporal extent - details' started"
22.11.2017 00:05:39 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
22.11.2017 00:05:39 - "Test Case 'Temporal extent - details' finished: PASSED"
22.11.2017 00:05:39 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
22.11.2017 00:05:39 - Releasing resources
22.11.2017 00:05:39 - Changed state from INITIALIZED to RUNNING
22.11.2017 00:05:39 - Duration: 15sec
22.11.2017 00:05:39 - TestRun finished
22.11.2017 00:05:39 - Changed state from RUNNING to COMPLETED
