31.01.2018 00:19:41 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDec7323d5-d8f0-4cfe-b23a-b826df86d58c__1 (initiated Wed Jan 31 00:19:41 CET 2018)
31.01.2018 00:19:41 - Resolving Executable Test Suite dependencies
31.01.2018 00:19:41 - Preparing 2 Test Task:
31.01.2018 00:19:41 -  TestTask 1 (16e90d4a-e4ed-49aa-9f57-b4e58382b2f3)
31.01.2018 00:19:41 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
31.01.2018 00:19:41 -  with parameters: 
31.01.2018 00:19:41 - etf.testcases = *
31.01.2018 00:19:41 -  TestTask 2 (ba5d506b-ab76-4da5-941a-0fb4d02d9ced)
31.01.2018 00:19:41 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
31.01.2018 00:19:41 -  with parameters: 
31.01.2018 00:19:41 - etf.testcases = *
31.01.2018 00:19:41 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 00:19:41 - Setting state to CREATED
31.01.2018 00:19:41 - Changed state from CREATED to INITIALIZING
31.01.2018 00:19:41 - Starting TestRun.5be9054b-16d7-44ca-9c27-6b92492ce55a at 2018-01-31T00:19:42+01:00
31.01.2018 00:19:42 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 00:19:42 - TestRunTask initialized
31.01.2018 00:19:42 - Creating new tests databases to speed up tests.
31.01.2018 00:19:42 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 00:19:42 - Optimizing last database etf-tdb-bd0deb74-0b02-487e-8716-fb16414bc313-0 
31.01.2018 00:19:42 - Import completed
31.01.2018 00:19:42 - Validation ended with 0 error(s)
31.01.2018 00:19:42 - Compiling test script
31.01.2018 00:19:42 - Starting XQuery tests
31.01.2018 00:19:43 - "Testing 1 records"
31.01.2018 00:19:43 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
31.01.2018 00:19:43 - "Statistics table: 1 ms"
31.01.2018 00:19:43 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
31.01.2018 00:19:43 - "Test Case 'Schema validation' started"
31.01.2018 00:19:44 - "Validating file GetRecordByIdResponse.xml: 1515 ms"
31.01.2018 00:19:44 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 1517 ms"
31.01.2018 00:19:44 - "Test Case 'Schema validation' finished: FAILED"
31.01.2018 00:19:44 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
31.01.2018 00:19:44 - Releasing resources
31.01.2018 00:19:44 - TestRunTask initialized
31.01.2018 00:19:44 - Recreating new tests databases as the Test Object has changed!
31.01.2018 00:19:44 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 00:19:44 - Optimizing last database etf-tdb-bd0deb74-0b02-487e-8716-fb16414bc313-0 
31.01.2018 00:19:44 - Import completed
31.01.2018 00:19:45 - Validation ended with 0 error(s)
31.01.2018 00:19:45 - Compiling test script
31.01.2018 00:19:45 - Starting XQuery tests
31.01.2018 00:19:45 - "Testing 1 records"
31.01.2018 00:19:45 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
31.01.2018 00:19:45 - "Statistics table: 1 ms"
31.01.2018 00:19:45 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
31.01.2018 00:19:45 - "Test Case 'Common tests' started"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.1: Title': PASSED - 1 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.2: Abstract': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 1 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Case 'Common tests' finished: FAILED"
31.01.2018 00:19:45 - "Test Case 'Hierarchy level' started"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
31.01.2018 00:19:45 - "Test Case 'Hierarchy level' finished: PASSED"
31.01.2018 00:19:45 - "Test Case 'Dataset (series) tests' started"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.c.1: Dataset identification': FAILED - 0 ms"
31.01.2018 00:19:45 - "Test Assertion 'md-iso.c.2: Dataset language': FAILED - 1 ms"
31.01.2018 00:19:45 - "Checking URL: 'https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service'"
31.01.2018 00:19:45 - "Checking URL: 'https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?typename=cp%3ACadastralZoning&amp;version=1.1.0&amp;request=GetFeature&amp;service=WFS'"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 49062 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.c.4: Dataset conformity': FAILED - 1 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.c.5: Dataset topic': FAILED - 0 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 0 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.c.7: Dataset lineage': FAILED - 0 ms"
31.01.2018 00:20:34 - "Test Case 'Dataset (series) tests' finished: FAILED"
31.01.2018 00:20:34 - "Test Case 'Service tests' started"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 1 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Case 'Service tests' finished: PASSED"
31.01.2018 00:20:34 - "Test Case 'Keywords' started"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.e.1: Keywords': FAILED - 0 ms"
31.01.2018 00:20:34 - "Test Case 'Keywords' finished: FAILED"
31.01.2018 00:20:34 - "Test Case 'Keywords - details' started"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 1 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Case 'Keywords - details' finished: PASSED"
31.01.2018 00:20:34 - "Test Case 'Temporal extent' started"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 1 ms"
31.01.2018 00:20:34 - "Test Case 'Temporal extent' finished: FAILED"
31.01.2018 00:20:34 - "Test Case 'Temporal extent - details' started"
31.01.2018 00:20:34 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
31.01.2018 00:20:34 - "Test Case 'Temporal extent - details' finished: PASSED"
31.01.2018 00:20:34 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
31.01.2018 00:20:35 - Releasing resources
31.01.2018 00:20:35 - Changed state from INITIALIZED to RUNNING
31.01.2018 00:20:35 - Duration: 54sec
31.01.2018 00:20:35 - TestRun finished
31.01.2018 00:20:35 - Changed state from RUNNING to COMPLETED
