var etsID,
urlToMetadata,
id,
lokatorZdroja,
serviceGetCap,
sietovaSluzba,
next,
featureTypeName,
identifikatorMD,
kontaktnaOrganizacia,
etsDataThemeTests,
icoIdentifikatorZdroja;
var datasetMetadata = "";
var datasets = "";
var etsInspireMetadataId = "EIDec7323d5-d8f0-4cfe-b23a-b826df86d58c";
var etsInspireDowSerDirWFS = "EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd";
var etsInspireMetadataInteroperabilty = "EID9a31ecfc-6673-43c0-9a31-b4595fb53a98";
var etsDataThemesAppSchemas = {
	"ad": ["EID8aaef94b-6a4d-47ab-a5d0-70ad5cb28b08", "EID9c31fa6e-1fab-4345-bf29-6d2c129de312", "EID334bbd38-378d-4a44-8a19-5d00df919ec0", "EID6985a681-fd81-4448-83e8-061758b9ca8c"],
	"cp":["EID1f9bc92a-5879-4e9b-bcbe-1d2d0cab0aab", "EID92032cdb-db88-42aa-96c0-70a1af9e68b1","EIDc4fbae00-3070-49fa-b803-24c66c31ac70","EIDdbcc48ae-6871-4444-8e95-547bc22aacb2","EID18b742d0-15eb-421f-bbec-7c8c5cf7ee1a"],
	"au": ["EIDddecef4b-89a3-4f9d-9246-a50b588fa5a2", "EIDacc5931c-4ff0-499f-b916-3cda1603456b", "EIDee28b75e-5c80-4370-838d-ab1b18e30b13", "EIDcafb75f8-5deb-4cca-89df-d3189322e97f"],
	"gn": ["EID0fc46305-c623-422b-b7d7-251c3b86eb7f", "EIDa32f76c7-f1d3-4d70-83ef-d51d2545fa2e", "EIDc3379b85-853e-4a35-8c3d-b64191d94587", "EID1620bd27-b881-48a2-bf2b-301541e035f4"],
	"ps": ["EID4c53a8c7-7cac-4531-982b-d03eb48ffa77", "EID7831c8b4-f666-4534-838a-137b30bfecbe", "EIDb529e8fa-b9f8-4758-acea-1d2af744599f", "EID828410c1-53f2-4683-bded-481ad9d4d3e9"],
	"ps-f": ["EID4c53a8c7-7cac-4531-982b-d03eb48ffa77", "EID7831c8b4-f666-4534-838a-137b30bfecbe", "EIDb529e8fa-b9f8-4758-acea-1d2af744599f", "EID828410c1-53f2-4683-bded-481ad9d4d3e9"],
	"hy-n": ["EIDe008001b-5233-4081-a1ae-515d7702ce02", "EIDd0b58f38-98ae-43a8-a787-9a5084c60267", "EID893b7541-c9cb-4e0a-9f84-5d55cad1866c", "EID122b2f38-302f-4271-9653-69cf86fcb5c4"],
	"hy-p": ["EID45133c90-1929-405c-867d-9648b0620bf7", "EIDd0b58f38-98ae-43a8-a787-9a5084c60267", "EID893b7541-c9cb-4e0a-9f84-5d55cad1866c", "EID122b2f38-302f-4271-9653-69cf86fcb5c4"],
	"au-mu": ["EID117562c2-d6e1-4345-9f7b-cba229cf6685", "EIDacc5931c-4ff0-499f-b916-3cda1603456b", "EIDee28b75e-5c80-4370-838d-ab1b18e30b13", "EIDee28b75e-5c80-4370-838d-ab1b18e30b13"],
	"tn": ["EID4441cbde-371f-4899-90b3-145f4fd08ebc", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"],
	"tn-a": ["EID6800c834-b4e0-4631-9209-73530fb9ccee", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"],
	"tn-c": ["EID731621b9-2daa-49fd-99ef-9279b7f335b5", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"],
	"tn-ra": ["EIDe2610a9f-6432-489d-8238-92b1193e7a3d", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"],
	"tn-ro": ["EID14986e54-74c4-43b0-979b-d0d3e5cd0e8c", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"],
	"tn-w": ["EIDeb35a20f-188d-4fd3-aee1-dd07eb3c3efa", "EID733af9a0-312b-4f71-9aa2-977cd2134d23", "EIDdf5db9a4-b15f-4193-a6ff-6e9951af46f5", "EID9d35024d-9dd7-43a9-afff-d5aea5f51595"]
};

// EID8aaef94b-6a4d-47ab-a5d0-70ad5cb28b08

$(document).keypress(function (e) {
	if (e.which == 13) {
		var targetId = $(e.target).attr("id");
		var targetVal = $(e.target).val();
		var optionValue = $('#inputValueOptions input:radio:checked').val();
		if (targetId == 'mdHttpUriInput') {
			if (optionValue == 1) {
				if (targetVal != '') {
					console.log("CALLING INSPIRE METADATA XML PARSER");
					parseMDUrl();
				} else {
					console.log("ADD URL TO INSPIRE METADATA XML");
					alert("Zadajte hodnotu do Web Adresa");
				}
			}
			if (optionValue == 2) {
				console.log("###### Calling function seachInGeoportal ######");
				searchInGeoportal(targetVal);
			}
			if (optionValue == 3) {
				console.log("###### Callling function searchInRpi ######");
				searchInRpi(targetVal);
			}
			if (optionValue == 4) {
				if (targetVal != '') {
					console.log("CALLING INSPIRE OGCWxS2ISOGMD FUNCTION");
					wxs2iso(targetVal);
				} else {
					console.log("ADD URL TO INSPIRE METADATA XML");
					alert("*** INFO ***\nZadajte URL na OGC WxS GetCapabilities");
				}
			}
		}
	}
});
function wxs2iso(url){
	$('#loaderImage').show();
	$.post("api/wxs2iso.php", url)
	.done(function (res) {
		var result = JSON.parse(res);
		if (result.status == 1){
			alert("#### INFO ####\nISO metadáta už sú dostupné v katalógu.\nPridávam do URL testovania.");
			$('#mdHttpUriInput').val(result.metadata);
			$('#optionManual').click();
		}
		else if(result.status == 0){
			alert("#### INFO ####\nISO metadáta boli vytvorené v katalógu.\nPridávam do URL testovania.");
			$('#mdHttpUriInput').val(result.metadata);
			$('#optionManual').click();
		}
		$('#loaderImage').hide();
	})
}
function searchInGeoportal(query) {
	console.log("###### Function searchInGeoportal has been called ######");
	$('#startButton').remove();
	$('#responseData').empty();
	$('#inspireZdroje').children().children().remove();
	$('#inspireZdroje').children().empty();
	$('#searchResults').empty();
	$('#loaderImage').show();
	var sendData = 'query={"type":["data","service"],"text":"' + query + '","scope":"AnyText","organisation":"","bbox":"","keywords":"","date_from":"","date_to":"","date_type":"Revision","modified_from":"","modified_to":"","inspire_theme":"","sort":"modified","topicCategory":"","serviceType":"","serviceKeyword":""}&limit=1500';
	$.post("api/queryGeo.php", sendData)
	.done(function (res) {
		var result = JSON.parse(res);
		var total = result.matches;
		var select = 'Total: ' + total + '<br><select id="resultSelect" class="form-control" onchange="getval(this,\'geo\');">';
		$.each(result.records, function (k, v) {
			select += ('<option value="' + v.id + '">' + v.title + ' (' + v.type + ')</option>');
		})
		select += '</select>'
		$('#searchResults').append(select);
		console.log(select);
		$('#loaderImage').hide();
	})
}
function searchInRpi(query) {
	console.log("###### Function searchInRpi has been called ######");
	$('#inspireZdroje').children().empty();
	$('#responseData').empty();
	$('#searchResults').empty();
	$('#loaderImage').show();
	$('#startButton').remove();
	if (query) {
		var url = 'api/proxy.php?url=https://rpi.gov.sk/client/map/api/sk/records/?anytext=' + query;
	} else {
		var url = 'api/proxy.php?url=https://rpi.gov.sk/client/map/api/sk/records/';
	}
	$.get(url, function (res) {
		var total = res.contents.total;
		console.log("RPI SEARCH TOTAL:" + total);
		var select = 'Total: ' + total + '<br><select id="resultSelect" class="form-control" onchange="getval(this,\'rpi\');">';
		console.log(res);
		limit = 15;
		next = res.contents.next;
		console.log(next);
		$.each(res.contents.records, function (k, v) {
			select += ('<option id="' + v.id + '" value="' + v.id + '">' + v.title + ' (' + v.type + ')</option>');
			if(total == 1){
				getval(v.id,'rpi');
			}
		})
		if (total > 15) {
			console.log("DORIESIT LOOPOVANIE GETOV");
			page = 1;
			pages = parseInt(total) / parseInt(limit);
			console.log("NUMBER OF LOOPS:" + pages);
			var urlNext = '';
			while (page <= pages) {
				$('#loaderImage').show();
				page = page + 1;
				console.log("PAGE:" + page);
				if (query) {
					urlNext = url.replace("records/?", "records/?page=" + page + "&");
				} else {
					urlNext = url + "?page=" + page;
				}
				$.get(urlNext, function (res) {
					$.each(res.contents.records, function (k, v) {
						//select += ('<option value="'+v.id+'">'+v.title+' ('+v.type+')</option>');
						$('#resultSelect').append('<option value="' + v.id + '">' + v.title + ' (' + v.type + ')</option>');
					})
				})
				$('#loaderImage').hide();
			}
		}
		select += '</select>';
		$('#searchResults').append(select);
		$('#loaderImage').hide();
	})
}
function getval(sel, api) {
	console.log(sel);
	if(typeof sel === "string" ){
		var uuid = sel;
	}
	else{
		var uuid = sel.value;
	}
	$('#startButton').remove();
	if (api == "geo") {
		var url = 'http://geoportal.gov.sk/sk/cat-client/detail.xml?uuid=' + uuid;
	}
	if (api == 'rpi') {
		var url = 'https://rpi.gov.sk/rpi_csw/service.svc/get?request=GetRecordById&service=CSW&version=2.0.2&elementSetName=full&outputschema=http://www.isotc211.org/2005/gmd&Id=' + uuid;
	}
	$('#mdHttpUriInput').val(url);
	$('#mdHttpUri').append('<button id="startButton" class="btn btn-info" onclick="parseMDUrl();">Hľadaj INSPIRE zdroje</button>');
	$('#optionManual').click();
}
function netSer(druhSluzby, lokatorZdroja) {
	if ((druhSluzby == 'download' || druhSluzby == 'OGC:WFS' || druhSluzby == 'OGC:WCS' || druhSluzby == 'OGC:SOS' || druhSluzby == 'WFS') && lokatorZdroja != '') {
		$("#inspireServices").append('<h4>Sieťové služby</h4>');
		sietovaSluzba = "<p>Typ: <b>" + druhSluzby + " </b></p><p>Pristupový bod: <b>" + lokatorZdroja + " </b>";
		$("#inspireServices").append(sietovaSluzba);
		
		if (lokatorZdroja.toLowerCase().indexOf("request=getcapabilities") < 0) {
			if (lokatorZdroja.indexOf("?") > 0){
				serviceGetCap = lokatorZdroja.substring(0, lokatorZdroja.indexOf("?")) + '?service=WFS&request=GetCapabilities';
			}
			else{
				serviceGetCap = lokatorZdroja + '?service=WFS&request=GetCapabilities';
			}
			
			console.log(serviceGetCap);
		}
		else{
			serviceGetCap = lokatorZdroja;
			console.log(serviceGetCap);
		}
		$('#loaderImage').show();
		$.get("api/proxy.php?url=" + encodeURIComponent(serviceGetCap), function (res) {
			$('#loaderImage').hide();
			console.log(res.status.http_code);
			$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
			if (res.status.http_code == 200) {
				getCapXML = $.parseXML(res.contents);
				var exception = $(getCapXML).find("Exception").attr("exceptionCode");
				var version = $(getCapXML).find("wfs\\:WFS_Capabilities,WFS_Capabilities").attr("version");
				var title = $(getCapXML).find("wfs\\:WFS_Capabilities,WFS_Capabilities").find("ows\\:Title,Title").first().text();
				$("#inspireServices").find("h4").after('<p>Názov: <b>'+title+'</b></p>');
				
				console.log("WFS version is: " + version);
				var featureTypes = $(getCapXML).find("wfs\\:WFS_Capabilities,WFS_Capabilities").find("wfs\\:FeatureTypeList,FeatureTypeList");
				var kvalitaSluzby = QOS(serviceGetCap, druhSluzby);
				if (featureTypes) {
					datasets += '<div>';
					$.each(featureTypes.find("wfs\\:FeatureType,FeatureType"), function (key, value) {
						featureTypeTitle = $(value).find("wfs\\:Title,Title").text();
						featureTypeName =  $(value).find("wfs\\:Name,Name").text();
						if ($(value)[0].attributes[0]){
							featureTypeSchemaName = $(value)[0].attributes[0].name;
							schemaCode = featureTypeSchemaName.split(':')[1];
							featureTypeSchemaUri = $(value)[0].attributes[0].nodeValue;
							getFeatureReq = lokatorZdroja.split("?")[0] + "?service=WFS&count=1&REQUEST=GetFeature&version=2.0.0&namespaces=xmlns("+schemaCode+","+featureTypeSchemaUri+")&TYPENAMES="+featureTypeName;
							
							datasets += '<p>Aplikačná schéma: <b>'+featureTypeSchemaName+'='+featureTypeSchemaUri+'</b><a href="'+getFeatureReq+'" target="_new"><span class="fa fa-list"></span></a></p>';
							//if (schemaCode == 'au' || schemaCode == 'ad' || schemaCode == 'gn' || schemaCode == 'ps' || schemaCode == 'ps-f' || schemaCode == 'hy-n' || schemaCode == 'hy-p' || schemaCode == 'au-mu' || schemaCode == 'tn' || schemaCode == 'tn-a' || schemaCode == 'tn-c' || schemaCode == 'tn-ra' || schemaCode == 'tn-ro' || schemaCode == 'tn-w'){
							if(etsDataThemesAppSchemas[schemaCode]){
								console.log(etsDataThemesAppSchemas[schemaCode]);
								etsDataThemeTests = etsDataThemesAppSchemas[schemaCode];
								console.log(etsDataThemeTests);
								etsDataThemeTests = etsDataThemeTests + "";
								var id = 'RUN__'+schemaCode+'__'+featureTypeName.split(":")[1]+'__'+etsDataThemeTests[0];
								datasets += '<button id="'+id+'" class="btn btn-primary mdTestRunButton" onclick="queryETFApi(\'' + etsDataThemeTests + '\',\'' + getFeatureReq + '\',\'' +id+ '\',\'' + featureTypeName + '\');">Test</button>';
								
							}
						}
						datasets += '<p>Dátová sada: <b>' + featureTypeTitle + '</b></p>';
						datasets += '<p>Strojove meno: <b>' + featureTypeName + '</b></p><hr>';
					})
					datasets += '</div>';
					$("#inspireData").append('<h4>Interoperabilita dát</h4>');
					$("#inspireData").append(datasets);
					//DATASET IDENTIFIERS
					var SpatialDataSetIdentifier = $(getCapXML).find("wfs\\:WFS_Capabilities,WFS_Capabilities").find("ows\\:ExtendedCapabilities,ExtendedCapabilities").find("inspire_dls\\:ExtendedCapabilities,ExtendedCapabilities").find("inspire_dls\\:SpatialDataSetIdentifier,SpatialDataSetIdentifier");
					console.log(SpatialDataSetIdentifier);
					if (SpatialDataSetIdentifier){
						$("#inspireServices p").last().append("<p>Nalinkovane datasety: </p>");
						$.each(SpatialDataSetIdentifier, function(k,v){
							inspire_common_Code = $(v).find("inspire_common\\:Code,Code").text();
							inspire_common_Namespace = $(v).find("inspire_common\\:Namespace,Namespace").text();
							$("#inspireServices p").last().append('<p>Identifikátor: <b>'+ inspire_common_Code + '</b>, menny priestor: <b>' + inspire_common_Namespace + '</b>');
							
						})
					}
				}
				console.log(getCapXML);
				$("#inspireServices p").first().append('<a href="' + serviceGetCap + '" target="_new"><span class="fa fa-list"></span></a>');
				$("#inspireServices").append('<button id="RUN__' + etsInspireDowSerDirWFS + '__2" class="btn btn-primary mdTestRunButton" onclick="queryETFApi(\'' + etsInspireDowSerDirWFS + '\',\'' + serviceGetCap +  '\',\'' + 'RUN__' + etsInspireDowSerDirWFS + '__2' + '\',\''+ title + '\');">Test</button></p>');
			} else {
				$("#inspireServices p").append('<a href="' + serviceGetCap + '"target="_new" class="btn btn-danger">' + res.status.http_code + '</a></p>');
			}
		})
	}
	else if ((druhSluzby == 'view' || druhSluzby == 'WMS' || druhSluzby == 'OGC:WMS') && lokatorZdroja != '') {
		$("#inspireServices").append('<h4>Sieťové služby</h4>');
		sietovaSluzba = "<p>Typ: <b>" + druhSluzby + " </b></p><p>Pristupový bod: <b>" + lokatorZdroja + " </b>";
		$("#inspireServices").append(sietovaSluzba);
		if (lokatorZdroja.toLowerCase().indexOf("request=getcapabilities") < 0) {
			serviceGetCap = lokatorZdroja + '?service=WMS&request=GetCapabilities';
			console.log(serviceGetCap);
		}
		else{
			serviceGetCap = lokatorZdroja;
			console.log(serviceGetCap);
		}
		$('#loaderImage').show();
		$.get("api/proxy.php?url=" + encodeURIComponent(lokatorZdroja), function (res) {
			console.log(res.status.http_code);
			$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
			if (res.status.http_code == 200) {
				$('#loaderImage').hide();
				$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
				var getCapXML = $.parseXML(res.contents);
				console.log(getCapXML);
				$("#inspireServices p").first().append('<a href="' + serviceGetCap + '" target="_new"><span class="fa fa-list"></span></a>');
				$("#inspireServices p").last().append('<button id="RUN__queryIGMVApi__2" class="btn btn-primary" onclick="queryIGMVApi(serviceGetCap,this.id);">Test</button></p>');
			}
			else {
				$('#loaderImage').hide();
				$("#inspireServices p").last().append('<a href="' + serviceGetCap + '"target="_new" class="btn btn-danger">' + res.status.http_code + '</a></p>');
			}
		})
	} else if (druhSluzby == 'other' && lokatorZdroja != '') {
		$("#inspireServices").append('<h4>Sieťové služby</h4>');
		sietovaSluzba = "<p>Typ: <b>" + druhSluzby + " </b></p><p>Pristupový bod: <b>" + lokatorZdroja + " </b>";
		$("#inspireServices").append(sietovaSluzba);
		$.get("api/proxy.php?url=" + encodeURIComponent(lokatorZdroja), function (res) {
			console.log(res.status.http_code);
			$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
			$("#inspireServices p").append('<a class="btn btn-warning" href="' + lokatorZdroja + '"><span class="fa fa-list"></span>' + druhSluzby + '</a></p>');
		})
	} else {
		$("#sdServices").append('<h4>Služby priestorových dát</h4>');
		sietovaSluzba = '<p>Typ: <b>' + druhSluzby + '</b></p><p>Pristupový bod: <b>' + lokatorZdroja + '  </b><a href='+lokatorZdroja+' target="_new"><span class="fa fa-list"></span></a>';
		$("#sdServices").append(sietovaSluzba);
	}
}
function parseMDUrl() {
	datasetMetadata = "";
	datasets = "";
	sietovaSluzba = "";
	icoIdentifikatorZdroja = "";
	kontaktnaOrganizacia = "";
	var url = $('#mdHttpUriInput').val();
	$('#loaderImage').show();
	$('#responseData').empty();
	$('#inspireZdroje').children().empty();
	console.log("###### Function parseMDUrl was called to handle URL: ######" + url);
	$.ajax({
		type : 'GET',
		url : 'api/proxy.php?url=' + encodeURIComponent(url),
	})
	.done(function parseXML(data) {
		$('#loaderImage').hide();
		$('#responseData').append("Odpovede zo serverov: <textarea>" + data.contents + "</textarea>");
		xml = $.parseXML(data.contents);
		$(xml).find("gmd\\:MD_Metadata, MD_Metadata").each(function () {
			var druhZdroja = $(this).find("gmd\\:MD_ScopeCode, MD_ScopeCode").first().text();
			identifikatorMD = $(this).find("gmd\\:fileIdentifier, fileIdentifier").first().text();
			kontaktnaOrganizacia = $(this).find("gmd\\:contact, contact").find("gmd\\:organisationName, organisationName").first().text();
			icoIdentifikatorZdroja = identifikatorMD.match(/([0-9]{8}(?=\/))/g);
			$("#inspireZdroje").first().prepend("<h3>Nájdené INSPIRE zdroje pre IČO: "+icoIdentifikatorZdroja+", Organizácia: "+kontaktnaOrganizacia+"</h3>");
			
			if (!druhSluzby) {
				druhZdroja = $(this).find("gmd\\:MD_ScopeCode, MD_ScopeCode").first().attr("codeListValue");
			}
			var nazovZdroja = $(this).find("gmd\\:identificationInfo,identificationInfo").find("gmd\\:title, title").first().text();

			// VECI PRE DATASET A DATASET SERIES
			if (druhZdroja != 'service') {
				var jidZdroja = $(this).find("gmd\\:MD_DataIdentification,MD_DataIdentification").find("gmd\\:RS_Identifier,RS_Identifier").find("gmd\\:code,code").find("gco\\:CharacterString,CharacterString").text();
				var jidNsZdroja = $(this).find("gmd\\:MD_DataIdentification,MD_DataIdentification").find("gmd\\:RS_Identifier,RS_Identifier").find("gmd\\:codeSpace,codeSpace").find("gco\\:CharacterString,CharacterString").text();
				lokatorZdroja = $(this).find("gmd\\:transferOptions,transferOptions").find("gmd\\:CI_OnlineResource,CI_OnlineResource");
				if (lokatorZdroja) {
					$.each(lokatorZdroja, function (key, value) {
						//alert(lokatorZdroja);
						var functionCode = $(value).find("gmd\\:CI_OnLineFunctionCode,CI_OnLineFunctionCode ").attr("codeListValue");
						var lokatorZdrojaValue = $(value).find("gmd\\:url,url").text();
						netSer(functionCode,lokatorZdrojaValue);
						//sietovaSluzba += "<pre>" + $(value).find("gmd\\:url,url").text() + " (<b>" + $(value).find("gmd\\:CI_OnLineFunctionCode,CI_OnLineFunctionCode ").attr("codeListValue") + "</b>) </pre>";
					})
					//sietovaSluzba += '</p>';
				}
			} else {
				// PARSOVANIE VECI PRE SLUZBU
				urlToMetadata = $('#mdHttpUriInput').val();
				//var connectPoint = $(this).find("srv\\:connectPoint,connectPoint").find("gmd\\:url,url").text();
				//var resourceLocator = var url = $(this).find("gmd\\:transferOptions,transferOptions").find("gmd\\:url,url").text();
				var jidZdroja = $(this).find("srv\\:SV_ServiceIdentification,SV_ServiceIdentification").find("gmd\\:RS_Identifier,RS_Identifier").find("gmd\\:code,code").find("gco\\:CharacterString,CharacterString").text();
				var jidNsZdroja = $(this).find("srv\\:SV_ServiceIdentification,SV_ServiceIdentification").find("gmd\\:RS_Identifier,RS_Identifier").find("gmd\\:codeSpace,codeSpace").find("gco\\:CharacterString,CharacterString").text();
				var druhSluzby = $(this).find("srv\\:serviceType,serviceType").find("gco\\:LocalName,LocalName").text();
				lokatorZdroja = $(this).find("gmd\\:transferOptions,transferOptions").find("gmd\\:url,url").text();
				var pouzivaneData = $(this).find("srv\\:operatesOn,operatesOn");
				if (pouzivaneData) {
					datasetMetadata += '<div id="operatesOnMetadata">Metadáta pre dátové sady ktoré služba používa: ';
					var number = 0
						$.each($(this).find("srv\\:SV_ServiceIdentification,SV_ServiceIdentification").find("srv\\:operatesOn,operatesOn"), function (key, value) {
							number = number + 1
							var title = $(value).attr("xlink:title");
							if (title == null){
								title = $(value).attr("uuidref")
							}
							var urlToMD = $(value).attr("xlink:href");
							id = 'RUN__' + etsInspireMetadataId + '__' + number;
							console.log(id);
							datasetMetadata += '<p><b>' + title + '</b><a href='+urlToMD+' target="_new"><span class="fa fa-list"></span></a></p><p>GetRecordById: <b>' + $(value).attr("xlink:href") + '</b></p>';
							$.get("api/proxy.php?url=" + encodeURIComponent(urlToMD), function (res) {
								console.log(res.status.http_code);
								$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
								if (res.status.http_code == 200) {
									$('#loaderImage').hide();
									$('#responseData').append('<p>HTTP Code: ' + res.status.http_code + '</p><textarea>' + res.contents + '</textarea>');
									var isoGMDXML = $.parseXML(res.contents);
									console.log(isoGMDXML);
									var gmdIdentifier = $(isoGMDXML).find("gmd\\:identifier,identifier");
									console.log(gmdIdentifier);
									if(gmdIdentifier.find("gmd\\:RS_Identifier,RS_Identifier")){
										$("#operatesOnMetadata p").first().append("<p>Identifikátor: <b>" + gmdIdentifier.find("gmd\\:code,code").find("gco\\:CharacterString,CharacterString").text()+ "</b><br>Menny priestor: <b>" + gmdIdentifier.find("gmd\\:codeSpace,codeSpace").find("gco\\:CharacterString,CharacterString").text() +"</b></p>");
									}
								}
								else {
									$('#loaderImage').hide();
									//$("#inspireServices p").last().append('<a href="' + serviceGetCap + '"target="_new" class="btn btn-danger">' + res.status.http_code + '</a></p>');
								}
							})
							datasetMetadata += '<button id="' + id + '" class="btn btn-primary mdTestRunButton" onclick="queryETFApi(\'' + etsInspireMetadataId + '\',\'' + urlToMD + '\',\'' + id + '\',\'' + title + '\');">Test</button>';
							datasetMetadata += '<hr>';

						})
						datasetMetadata += '</div>';
				};
				if (druhSluzby != '' && lokatorZdroja != '') {
					netSer(druhSluzby,lokatorZdroja);
				};
			}
			id = 'RUN__' + etsInspireMetadataId + '__0';
			urlToMetadata = $('#mdHttpUriInput').val();
			$("#inspireMetadata").append('<h4>Metadáta</h4>');
			$("#inspireMetadata").append('<p>INSPIRE metadátovy záznam pre druh zdroja: <b>' + druhZdroja + '</b></p><p>Názov: <b>' + nazovZdroja + '</b><a href='+urlToMetadata+' target="_new"><span class="fa fa-list"></span></a></p><p>GetRecordById: <b>'+urlToMetadata+'</b><button id="' + id + '" class="btn btn-primary mdTestRunButton" onclick="queryETFApi(\'' + etsInspireMetadataId + '\',\'' + urlToMetadata + '\',\'' + id + '\',\'' + nazovZdroja + '\');">Test</button></p>');
			$("#inspireMetadata").append(datasetMetadata);
			$("#inspireMetadata p").first().append("<p>Identifikátor: <b>" +jidZdroja+ "</b><br>Menny priestor: <b>" +jidNsZdroja+"</b></p>");
		

			$(xml).find("gmd\\:onLine, onLine").each(function () {
				var url = $(this).find("gmd\\:transferOptions,transferOptions").find("gmd\\:url,url").text();
				$("#inspireMetadata").append("<p>" + url + "</p>");
			});
		})
	})
	.fail(function (e) {
		alert("CHYBA NA URL ADRESE: " + url + " STATUS: " + e.status + " STATUS TEXT: " + e.statusText);
		console.log("CHYBA NA URL ADRESE: " + url + " STATUS: " + e.status + " STATUS TEXT: " + e.statusText);
	})
}
function queryETFApi(etsId, url, elementID,title) {
	//var postData = '';
	//var confClass = '';
	console.log("####### FUNCTION queryETFApi CALLED ######");
	console.log(etsId);
	console.log(url);
	console.log(elementID);
	console.log("title: "+title);
	console.log("icoIdentifikatorZdroja: "+icoIdentifikatorZdroja);
	console.log('kontaktnaOrganizacia: '+kontaktnaOrganizacia.replace(/(\r\n|\n|\r)/gm,"").replace(/ /g,''));
	$('#' + elementID).html('<i class="fa fa-spinner fa-spin"></i> ETF testuje ...');
	$('#testResultButton').remove();
	//var etfTestRunURL = 'http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns';
	var now = $.now();
	var etsIDArray = etsId.split(",");
	console.log("ETSIDARRAYLENGTH: ", etsIDArray.length);
	console.log("ETSID: ", etsId);
	if (etsId == etsInspireDowSerDirWFS){
		var confClass = "downloadService_"+icoIdentifikatorZdroja+"_"+kontaktnaOrganizacia.replace(/(\r\n|\n|\r)/gm,"").replace(/ /g,'')+"_"+title;
		//var confClass = "sk_testing_inspire_resources_app__" + etsID + "__" + elementID;
		var postData = '{"label": "' + confClass+'","executableTestSuiteIds":' + JSON.stringify(etsIDArray) + ',"arguments": {},"testObject": {"resources": {"serviceEndpoint": "' + url + '"}}}';

	}
	else if (etsId == etsInspireMetadataId){
		var confClass = "metadata_"+icoIdentifikatorZdroja+"_"+kontaktnaOrganizacia.replace(/(\r\n|\n|\r)/gm,"").replace(/ /g,'')+"_"+title;
		//var confClass = "sk_testing_inspire_resources_app__" + etsID + "__" + elementID;
		var postData = '{"label": "' + confClass+'","executableTestSuiteIds":' + JSON.stringify(etsIDArray) + ',"arguments": {},"testObject": {"resources": {"data": "' + url + '"}}}';

	}	
	//else if (etsIDArray.length > 1){
		else{
		var confClass = "interoperability_"+icoIdentifikatorZdroja+"_"+kontaktnaOrganizacia.replace(/(\r\n|\n|\r)/gm,"").replace(/ /g,'')+"_"+title;
		//var confClass = "sk_testing_inspire_resources_app__" + etsID + "__" + elementID;
		var postData = '{"label": "' + confClass+'","executableTestSuiteIds":' + JSON.stringify(etsIDArray) + ',"arguments": {},"testObject": {"resources": {"data": "' + url + '"}}}';

	}
	//var sendData = '{"label": "' + confClass+'","executableTestSuiteIds":' + JSON.stringify(etsIDArray) + ',"arguments": {},"testObject": {"resources": {"data": "' + url + '"}}}';
	//$.post("api/queryETF.php", sendData)
	console.log("POST DATA: ", postData);
	$.ajax({
		url:"api/queryETF.php",
		type:"POST",
		data:postData,
		contentType:"application/json",
		dataType:"json"})
	.done(function (res) {
		$('#' + elementID).html("OK");
		console.log(res);
		//var result = JSON.parse(res);
		var resultId = res.id;
		var resultStatus = res.status;
		var html = res.html;
		var txt = res.txt;

		if (!resultId || resultId == null) {
			$('#' + elementID).before('<button id="testResultButton__' + elementID + '" class="btn btn-danger">Vysledok: <span class="badge">0</span></button>');
			$('#responseData').append("<textarea>" + JSON.stringify(res) + "</textarea>");
		} else {
			$('#' + elementID).after('<a id="testResultButtonReport__' + elementID + '" class="btn btn-success btn-xs" href="' + html + '" target="_blank">Report</a>');
			$('#' + elementID).after('<a id="testResultButtonLog__' + elementID + '" class="btn btn-info btn-xs" href="' + txt + '" target="_blank">Log</a>');
			$('#responseData').append("<textarea>" + JSON.stringify(res) + "</textarea>");
		}
	})
}

function queryIGMVApi(url,elementID) {
	console.log("URL NA TEST: " + url);
	$('#' + elementID).html('<i class="fa fa-spinner fa-spin"></i> Geoportal testuje ...');
	$('#testResultButton').remove();
	console.log(url);
	console.log(elementID);
	//var etfTestRunURL = 'http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns';
	var now = $.now();
	$.post("api/queryIGMVApi.php", url)
	.done(function (res) {
		$('#' + elementID).html("OK");
		var result = JSON.parse(res);
		var html = result.html;
		$('#' + elementID).after('<a id="testResultButtonReport__' + elementID + '" class="btn btn-success btn-xs" href="' + html + '" target="_blank">Report</a>');
		$('#responseData').append("<textarea>" + res + "</textarea>");
		}
	)
}
function QOS(serviceEndpoint, type) {

	return "<p>Normalizovaná testovacia procedúra pre: " + serviceEndpoint + "</p>";

}
function ulozZostavu(){
	alert("*** Ukladam zostavu do HTML");
	$.ajax({
		url:"api/saveHTML.php",
		type:"POST",
		data:$('html')[0].outerHTML
		})
	.done(function (res) {
		window.open("http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/html/" + res + ".html", '_blank');
	})
}
// VECI CO SA MAJU DAAT HNED KED SA NALOADUJE DOM
$(document).ready(function () {
	$('#optionRPI').click(function () {
		$('#startButton').remove();
		$('#mdHttpUriInput').val('');
	})
})
